﻿using ApiRecomSystm.Logic.Moduls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ApiRecomSystm.GUI.Windows
{
	/// <summary>
	/// Interaction logic for AddModulWindow.xaml
	/// </summary>
	public partial class AddModulWindow : Window
	{
		public AkinyiModuleInfo modul { get; set; } = new AkinyiModuleInfo();

		void InternalInit()
		{
			EnableComb.ItemsSource = new List<string> { "True", "False" };
		}

		public AddModulWindow(AkinyiModuleInfo outMod)
		{
			InitializeComponent();
			InternalInit();
			DataContext = outMod;
			modul = outMod;
			modul.PropertyChanged += Modul_PropertyChanged;
		}

		private void Modul_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			LstView.ItemsSource = modul.GetTags;
			CombTag.ItemsSource = modul.GetTags;
		}

		public AddModulWindow()
		{
			InitializeComponent();
			InternalInit();
			modul.PropertyChanged += Modul_PropertyChanged;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}

		private void BtnClickAddTag(object sender, RoutedEventArgs e)
		{
			if(CombTag.Text != null && CombTag.Text.Length > 0)
			{
				var tgs = modul.GetTags;

				if(tgs.Any((v) => v != CombTag.Text))
				{
					modul.AddTag(CombTag.Text);
				}
			}

			CombTag.Text = "";
		}

		private void BtnClickRemoveTag(object sender, RoutedEventArgs e)
		{
			if (LstView.SelectedItem != null)
			{
				modul.RemoveTag(LstView.SelectedItem.ToString());
			}
			else if(CombTag.Text != null && CombTag.Text.Length > 0)
			{
				var tgs = modul.GetTags;

				if (tgs.Any((v) => v == CombTag.Text))
				{
					modul.RemoveTag(CombTag.Text);

					CombTag.Text = "";
				}
			}

		}
	}
}
