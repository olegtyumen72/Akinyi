﻿using PhysicModule.Helpers;
using PhysicModule.Interfaces;
using PhysicModule.Models;
using PhysicModule.Models.Text;
using PhysicModule.Moduls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using SD = System.Drawing;

namespace PhysicModule.ModelView
{

	public class FieldSchemeViewModel
	{
		public event RoutedEventHandler Loaded;
		public event Action TaskStart;
		public event Action NewStep;
		public event Action TaskEnd;
		public event Func<List<DrawElementBase>, TopicTask ,bool> CheckTask;

		bool debug = false;
		bool killViewPort = false;

		#region Templates 

		Shape temlateLine { get; set; } = new Line()
		{
			Stroke = Brushes.LightGray,
			StrokeThickness = 3,
		};
		Ellipse templateElp { get; set; } = new Ellipse()
		{
			Width = 10,
			Height = 10,
			Stroke = Brushes.Red,
			StrokeThickness = 0,
			Fill = Brushes.Red
		};

		Rect templateElips { get; set; } = new Rect() { Width = 10, Height = 10 };

		#endregion

		Viewport vp;

		//standart settings
		double offsetX;
		double offsetY;
		double step = 40;

		//view settings and field
		Rect userWindow = new Rect()
		{
			Width = 400,
			Height = 300
		};
		Rect field = new Rect()
		{
			Width = 800,
			Height = 600
		};
		public double FieldWidth
		{
			//get { return SmartCanvas.ActualWidth; }
			//get => SmartCanvas.Width;
			//set => SmartCanvas.Width = value; 
			get => field.Width;
			set => field.Width = value;
		}
		public double FieldHeight
		{
			//get { return SmartCanvas.ActualHeight; }
			//get => SmartCanvas.Height;
			//set => SmartCanvas.Height = value;
			get => field.Height;
			set => field.Height = value;
		}

		//field settings
		/// <summary>
		/// 0 - width, 1 - height
		/// </summary>
		Point[,] pointField;
		/// <summary>
		/// 0 - width, 1 - height
		/// </summary>
		bool[,] pointStatus;
		int HeightPoints { get { return pointStatus.GetLength(1); } }
		int WidthPoints { get { return pointStatus.GetLength(0); } }

		internal void RouteCure()
		{
			if(first != null)
			{
				first.Item.IsVertical = !first.Item.IsVertical;
				UpdateField(vp);
			}
		}

		//view and elements
		Canvas SmartCanvas
		{
			get;
			set;
		}
		List<DrawElementBase> Elements { get; set; } = new List<DrawElementBase>();

		//element setting
		int elemSegCountW = 4;
		int ElemPntCntW { get { return elemSegCountW + 1; } }
		int elemSegCountH = 4;
		int ElemPntCntH { get { return elemSegCountH + 1; } }

		public FieldSchemeViewModel()
		{
			SmartCanvas = new Canvas()
			{
				Tag = "SMART_CANVAS\\SMART_CANVAS\\SMART_CANVAS\\SMART_CANVAS",
				Background = Brushes.Transparent,
				IsHitTestVisible = true
			};

			SmartCanvas.MouseLeftButtonDown += SmartCanvas_MouseDown_StartDrag;

			SmartCanvas.SizeChanged += (v, v1) =>
			{
				var old = Viewport.Clone(vp);
				vp = new Viewport(new RectWrapper(userWindow), new RectWrapper(SmartCanvas.PanelToRect()), true);

				UpdateField(old, true);
			};

			SmartCanvas.MouseWheel += SmartCanvas_MouseWheel_Zoom;


			if (debug)
			{
				offsetX = 0;
				offsetY = 0;
				SmartCanvas.Background = Brushes.Blue;
				FieldWidth = 100;
				FieldHeight = 100;
				step = 20;
				elemSegCountW = 2;
				elemSegCountH = 2;
				userWindow.X = -100;
				userWindow.Y = -100;
				if (killViewPort)
				{
					userWindow.Width = FieldWidth;
					userWindow.Height = FieldHeight;
					SmartCanvas.Width = FieldWidth;
					SmartCanvas.Height = FieldHeight;
				}
			}
		}

		private void SmartCanvas_MouseWheel_Zoom(object o, MouseWheelEventArgs a)
		{
			int delta = 50;

			if (a.Delta > 0)
			{
				if (userWindow.Width - delta > 0)
					userWindow.Width -= delta;
				if (userWindow.Height - delta > 0)
					userWindow.Height -= delta;
			}
			else
			{
				userWindow.Width += delta;
				userWindow.Height += delta;
			}

			var old = vp;

			vp = new Viewport(new RectWrapper(userWindow), new RectWrapper(SmartCanvas.PanelToRect()), true);

			UpdateField(old, true);
		}

		/// <summary>
		/// Вызывается из связанной вьюхи
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="isNew"></param>
		public void InitControl(Panel doc = null)
		{
			if (doc != null)
			{
				doc.Children.Add(SmartCanvas);

				if (killViewPort)
				{
					doc.Width = 100; doc.Height = 100;
					vp = new Viewport(new RectWrapper(userWindow), new RectWrapper(new Rect(0, 0, doc.Width, doc.Height)), true);
				}
				else
				{
					vp = new Viewport(new RectWrapper(userWindow), new RectWrapper(new Rect(0, 0, doc.ActualWidth, doc.ActualHeight)), true);
				}
			}

			InitField(doc != null);
			DrawGrid();
			Loaded?.Invoke(this, new RoutedEventArgs());
		}

		void UpdateField(Viewport old, bool isZoom = false)
		{
			SmartCanvas.Children.Clear();
			DrawGrid();
			DrawElemnts(old, isZoom);
		}

		private void DrawElemnts(Viewport old, bool isZoom)
		{
			foreach (var elem in Elements)
			{
				Point topL = elem.View.GetPosition(),
					topLWord = old.ConvertToWorld(topL);
				int col = FindCol(topLWord),
					row = FindRow(topLWord);
				if (col < 0 || row < 0) continue;
				//находим верхнюю и нижнюю реальные точку
				topL = vp.ConvertToPlane(topLWord);
				Point botR = vp.ConvertToPlane(pointField[col + elemSegCountW, row + elemSegCountH]);
				//стандартные точки для отрисовки элемента (первый квадрат)


				elem.ScrnInp = vp.ConvertToPlane(old.ConvertToWorld(elem.ScrnInp));
				elem.ScrnOut = vp.ConvertToPlane(old.ConvertToWorld(elem.ScrnOut));

				Canvas qwe;

				if (isZoom)
				{
					elem.ActualWidth = botR.X - topL.X;
					elem.ActualHeight = botR.Y - topL.Y;

					var tmpVP = new Viewport((RectWrapper)vp.World.Clone(), (RectWrapper)vp.Plane.Clone(), vp.IsConverEqual);
					tmpVP.World.X = 0;
					tmpVP.World.Y = 0;

					Point inputPtr = tmpVP.ConvertToPlane(pointField[0, elemSegCountH / 2]),
						outputPtr = tmpVP.ConvertToPlane(pointField[elemSegCountW, elemSegCountH / 2]);

					qwe = elem.GetView(inputPtr, outputPtr, 0, -1, true);
				}
				else
				{
					qwe = elem.View;
				}

				qwe.SetPosition(topL);
				SmartCanvas.Children.Add(qwe);
			}

			foreach (var elem in Elements)
			{

				if (elem.InputUICon != null)
				{
					var connection = GetConnectionOrNull(elem.InputUICon.ConObj, elem.InputUI);
					if (connection != null)
					{
						ConnectDrawable(new SelectedItem(elem, connection.Connecter), new SelectedItem(elem.InputUICon.ConObj, elem.InputUICon.Connecter));
					}
					else
					{
						elem.InputUICon = null;
					}
				}

				if (elem.OutputUICon != null)
				{
					var connection = GetConnectionOrNull(elem.OutputUICon.ConObj, elem.OutputUI);
					if (connection != null)
					{
						ConnectDrawable(new SelectedItem(elem, connection.Connecter), new SelectedItem(elem.OutputUICon.ConObj, elem.OutputUICon.Connecter));
					}
					else
					{
						elem.InputUICon = null;
					}
				}

			}
		}

		private void ConnectDrawable(SelectedItem item1, SelectedItem item2)
		{
			var canvPosF = (item1.Selection.Parent as Canvas).GetPosition();
			var canvPosS = (item2.Selection.Parent as Canvas).GetPosition();
			var posF = item1.Selection.GetPosition();
			var posS = item2.Selection.GetPosition();

			var realPosF = new Point(canvPosF.X + posF.X + item1.Selection.Width / 2, canvPosF.Y + posF.Y + item1.Selection.Height / 2);
			var realPosS = new Point(canvPosS.X + posS.X + item2.Selection.Width / 2, canvPosS.Y + posS.Y + item2.Selection.Height / 2);

			if ( (realPosF.X == realPosS.X && item1.Item.IsVertical == item2.Item.IsVertical == true) ||
				 (realPosF.Y == realPosS.Y && item1.Item.IsVertical == item2.Item.IsVertical == false))
			{
				var el = temlateLine.CopyTo(
					Booster.ConnectPoints(realPosF, realPosS)
					);
				el.Stroke = Brushes.Red;

				Func<object, bool> comparer = (v) =>
				{
					var tmp = v as Line;
					if (tmp != null && v == el)
					{
						return true;
					}
					if (tmp != null && tmp.CompareSwapLine(el as Line))
					{
						return true;
					}
					else
					{
						return false;
					}
				};

				if (SmartCanvas.FirstOrNull(comparer) != null) return;
				SmartCanvas.Children.Add(el);
			}		


		}

		private ConnectedDrawable GetConnectionOrNull(DrawElementBase dest, Shape src)
		{
			if (dest.InputUICon != null && dest.InputUICon.Connecter == src)
				return dest.InputUICon;
			else if (dest.OutputUICon != null && dest.OutputUICon.Connecter == src)
				return dest.OutputUICon;
			else
				return null;
		}

		private void DrawGrid()
		{
			List<List<Point>> drawPoints = new List<List<Point>>();
			Point viewLTop = new Point(userWindow.X, userWindow.Y),
				viewRBot = new Point(userWindow.X + userWindow.Width, userWindow.Y + userWindow.Height);
			int widthP = pointField.GetLength(0),
				heightP = pointField.GetLength(1);

			for (int j = 0; j < heightP; j++)
			{
				var curLine = new List<Point>();
				drawPoints.Add(curLine);

				for (int i = 0; i < widthP; i++)
				{
					var curPoint = pointField[i, j];


					if (PointInUserWindow(curPoint, viewLTop, viewRBot))
					{
						curLine.Add(curPoint);
					}

					if (curPoint.CompareTo(viewLTop) == -1)
					{
						Point nextColPtr = new Point(-1, -1),
							nextRowPtr = new Point(-1, -1);

						if (i + 1 < widthP)
						{
							nextColPtr = pointField[i + 1, j];
						}
						if (j + 1 < heightP)
						{
							nextRowPtr = pointField[i, j + 1];
						}

						if (!nextColPtr.IsEmpty() && PointInUserWindow(nextColPtr, viewLTop, viewRBot))
						{
							curLine.Add(curPoint);
						}
						else if (!nextRowPtr.IsEmpty() && PointInUserWindow(nextRowPtr, viewLTop, viewRBot))
						{
							curLine.Add(curPoint);
						}
					}

					if (curPoint.CompareTo(viewRBot) == 1)
					{
						Point befColPtr = new Point(-1, -1),
							befRowPtr = new Point(-1, -1);

						if (i - 1 > -1)
						{
							befColPtr = pointField[i - 1, j];
						}
						if (j - 1 > -1)
						{
							befRowPtr = pointField[i, j - 1];
						}

						if (!befColPtr.IsEmpty() && PointInUserWindow(befColPtr, viewLTop, viewRBot))
						{
							curLine.Add(curPoint);
						}
						else if (!befRowPtr.IsEmpty() && PointInUserWindow(befRowPtr, viewLTop, viewRBot))
						{
							curLine.Add(curPoint);
						}
					}
				}
			}

			#region old
			//if (pointField == null || pointField.GetLength(0) <= 0 || pointField.GetLength(1) <= 0) return;
			//int n = 0,
			//	countX = pointField.GetLength(0),
			//	countY = pointField.GetLength(1);
			//bool isXEnd = false,
			//	isYEnd = false;


			#endregion

			int n = 0,
				countY = pointField.GetLength(1),
				countX = pointField.GetLength(0);
			bool isXEnd = false,
				isYEnd = false;

			while (true)
			{
				if (n < countX)
				{
					SmartCanvas.Children.Add(
						temlateLine.CopyTo(
							Booster.ConnectPoints(
							vp.ConvertToPlane(pointField[n, 0]),
								vp.ConvertToPlane(pointField[n, countY - 1])
								)));
				}
				else
				{
					isXEnd = true;
				}

				if (n < countY)
				{
					SmartCanvas.Children.Add(
						temlateLine.CopyTo(
							Booster.ConnectPoints(
								vp.ConvertToPlane(pointField[0, n]),
								vp.ConvertToPlane(pointField[countX - 1, n])
								)));
				}
				else
				{
					isYEnd = true;
				}

				if (isYEnd && isXEnd)
				{
					break;
				}
				else
				{
					isYEnd = false;
					isXEnd = false;
					n++;
				}

			}
		}

		bool PointInUserWindow(Point p, Point viewLTop, Point viewRBot)
		{
			if (p.CompareTo(viewLTop) >= 0 && p.CompareTo(viewRBot) <= 0)
			{
				return true;
			}

			return false;
		}

		bool PointInUserWindow(Point p)
		{
			Point viewLTop = new Point(userWindow.X, userWindow.Y),
				viewRBot = new Point(userWindow.X + userWindow.Width, userWindow.Y + userWindow.Height);

			if (p.CompareTo(viewLTop) >= 0 && p.CompareTo(viewRBot) <= 0)
			{
				return true;
			}

			return false;
		}

		private void InitField(bool isNew)
		{
			if (double.IsNaN(FieldHeight) || double.IsNaN(FieldWidth)) return;
			//scaleRate = (FieldHeight > FieldWidth ? FieldHeight : FieldWidth ) *0.1;
			SmartCanvas.Children.Clear();

			double markedH = 0,
			markedW = 0;


			int countW = (int)(FieldWidth / step) + 1,
			 countH = (int)(FieldHeight / step) + 1;

			pointField = new Point[countW, countH];
			pointStatus = new bool[countW, countH];

			markedW = offsetY;
			for (int i = 0; i < countW; i++)
			{
				markedH = offsetX;
				for (int j = 0; j < countH; j++)
				{
					pointField[i, j] = new Point(markedW, markedH);
					pointStatus[i, j] = false;
					markedH += step;
				}
				markedW += step;
			}

			//дебаг
			if (debug)
			{

				var eli = new Ellipse()
				{
					Width = 20,
					Height = 20,
					Stroke = Brushes.Red,
					StrokeThickness = 1,
					Fill = Brushes.Red
				};

				eli.SetShiftedPosition(pointField[WidthPoints - 2, HeightPoints - 2]);
				SmartCanvas.Add(eli);

				eli = new Ellipse()
				{
					Width = 20,
					Height = 20,
					Stroke = Brushes.Red,
					StrokeThickness = 1,
					Fill = Brushes.Red
				};

				eli.SetShiftedPosition(pointField[1, 1]);
				SmartCanvas.Add(eli);
			}
		}

		object MvObj = null;
		Point POldMvObj = new Point(-1, -1);
		SelectedItem first;
		SelectedItem secnd;

		private void SmartCanvas_MouseDown_StartDrag(object o, MouseButtonEventArgs a)
		{
			var tmp = a.Source;
			if (tmp is Shape && !(tmp is Ellipse))
			{
				tmp = (a.OriginalSource as Shape).Parent;
			}

			//перемещение элемента
			if (tmp is Canvas && tmp != SmartCanvas)
			{
				var canv = tmp as Canvas;
				MvObj = canv;
				POldMvObj = vp.ConvertToWorld(
					canv.GetPosition()
					);
				var dragOffset = Point.Subtract(
									a.GetPosition(SmartCanvas),
									canv.GetPosition());


				MouseEventHandler mover = (vv, oo) =>
				{
					if (MvObj != null)
					{
						var canv_m = MvObj as Canvas;

						FreeSpace(FindIndexs(POldMvObj));

						var pos =
							ClosestPoint(
								vp.ConvertToWorld(GetShiftedPosition(dragOffset, oo.GetPosition(SmartCanvas), SmartCanvas, canv_m))
							);

						if (POldMvObj != pos)
						{

							FillSpace(FindIndexs(pos));
							canv_m.SetPosition(
								vp.ConvertToPlane(pos)
								);
							POldMvObj = pos;
							UpdateField(vp);
							//if (debug)
							//{
							//	(canv_m.Parent as Border).SetPosition(pos);
							//}
						}
						else
						{
							FillSpace(FindIndexs(POldMvObj));
						}
					}
				};
				MouseButtonEventHandler stopDrag = null;

				stopDrag = (vv, oo) =>
				{
					MvObj = null;
					SmartCanvas.MouseMove -= mover;
					SmartCanvas.MouseLeftButtonUp -= stopDrag;
				};

				SmartCanvas.MouseMove += mover;
				SmartCanvas.MouseLeftButtonUp += stopDrag;
			}

			//перемещение окна пользователя по сетке
			if (tmp == SmartCanvas)
			{
				var str = vp.ConvertToWorld(a.GetPosition(SmartCanvas));

				MouseEventHandler mover = (vv, oo) =>
				{
					var ptr = vp.ConvertToWorld(oo.GetPosition(SmartCanvas));
					var delta = ptr - str;
					var range = MathTools.PointsRange(ptr, str);
					var old = Viewport.Clone(vp);

					userWindow.X -= delta.X / 2;
					userWindow.Y -= delta.Y / 2;
					vp = new Viewport(new RectWrapper(userWindow), new RectWrapper(SmartCanvas.PanelToRect()), true);

					UpdateField(old);

					str = ptr;
				};

				MouseButtonEventHandler stopDrag = null;

				stopDrag = (vv, oo) =>
				{
					SmartCanvas.MouseMove -= mover;
					SmartCanvas.MouseLeftButtonUp -= stopDrag;
				};

				SmartCanvas.MouseMove += mover;
				SmartCanvas.MouseLeftButtonUp += stopDrag;
			}

			//соединение элементов
			if (tmp is Ellipse)
			{
				var mainCanv = (tmp as Shape).Parent as Canvas;
				var elip = tmp as Ellipse;

				if (first != null && first.Selection == elip) return;

				if (first != null)
				{
					secnd = new SelectedItem()
					{
						Item = Elements.Find((v) => v.View == mainCanv),
						Selection = elip
					};
					SelectElipse(elip);
				}

				if (first == null)
				{
					first = new SelectedItem()
					{
						Item = Elements.Find((v) => v.View == mainCanv),
						Selection = elip
					};
					SelectElipse(elip);
				}

				if (first != null && secnd != null)
				{
					ConnectDrawElements(first, secnd);
					Diselect(first);
					Diselect(secnd);
					first = null;
					secnd = null;
					UpdateField(vp);
					NewStep?.Invoke();
				}
			}
		}

		private void ConnectDrawElements(SelectedItem lfirst, SelectedItem lscnd)
		{
			DisconectSelectable(lfirst);
			DisconectSelectable(lscnd);

			ConnectSelected(lfirst, lscnd);
			ConnectSelected(lscnd, lfirst);

		}

		private void ConnectSelected(SelectedItem src, SelectedItem dest)
		{
			if (src.Selection == src.Item.InputUI)
			{
				src.Item.InputUICon = new ConnectedDrawable()
				{
					ConObj = dest.Item,
					Connecter = dest.Selection,
					ConType = dest.Selection == dest.Item.InputUI ? PointType.Inp : PointType.Outp
				};
			}
			else
			{
				src.Item.OutputUICon = new ConnectedDrawable()
				{
					ConObj = dest.Item,
					Connecter = dest.Selection,
					ConType = dest.Selection == dest.Item.InputUI ? PointType.Inp : PointType.Outp
				};
			}
		}

		void DisconectSelectable(SelectedItem selItem)
		{
			if (selItem.Selection == selItem.Item.InputUI)
			{
				if (selItem.Item.InputUICon != null)
				{
					DisconectConect(selItem.Item.InputUICon, selItem.Item);
				}
			}

			if (selItem.Selection == selItem.Item.OutputUI)
			{
				if (selItem.Item.OutputUICon != null)
				{
					DisconectConect(selItem.Item.OutputUICon, selItem.Item);
				}
			}
		}

		private void DisconectConect(ConnectedDrawable connection, DrawElementBase src)
		{
			if (connection.ConType == PointType.Inp)
			{
				if (connection.ConObj.InputUICon.ConObj == src)
				{
					connection.ConObj.InputUICon = null;
				}
			}
			else
			{
				if (connection.ConObj.OutputUICon.ConObj == src)
				{
					connection.ConObj.OutputUICon = null;
				}
			}
		}

		private void Diselect(SelectedItem si)
		{
			if (si.Selection is Ellipse)
			{
				DiselectElipse(si.Selection);
			}
		}

		void SelectElipse(Shape el)
		{
			el.Stroke = Brushes.Black;
			el.Fill = Brushes.Transparent;
		}

		void DiselectElipse(Shape el)
		{
			el.Fill = Brushes.Purple;
			el.Stroke = Brushes.Purple;
		}

		private Point ClosestPoint(Point point)
		{
			var res = new Dictionary<Point, double>();
			var rangeMin = double.MaxValue;
			var ptrRes = new Point(-1, -1);

			foreach (var ptr in pointField)
			{
				if (CheckSpace(FindCol(ptr), FindRow(ptr)))
				{
					var range = MathTools.PointsRange(point, ptr);
					if (rangeMin > range)
					{
						ptrRes = ptr;
						rangeMin = range;
					}
				}
			}

			return ptrRes;
		}

		Point FindIndexs(Point ptr)
		{
			return new Point(FindCol(ptr), FindRow(ptr));
		}

		int FindRow(Point ptr)
		{
			return (int)Math.Round(ptr.Y / step);
		}

		int FindCol(Point ptr)
		{
			return (int)Math.Round(ptr.X / step);
		}

		void FillSpace(Point indx)
		{
			FillSpace((int)indx.X, (int)indx.Y);
		}

		private void FillSpace(int col, int row)
		{
			for (int i = col; i < col + ElemPntCntW; i++)
			{
				for (int j = row; j < row + ElemPntCntH; j++)
				{
					pointStatus[i, j] = true;
				}
			}
		}

		void FreeSpace(Point indx)
		{
			FreeSpace((int)indx.X, (int)indx.Y);
		}

		private void FreeSpace(int col, int row)
		{
			for (int i = col; i < col + ElemPntCntW; i++)
			{
				for (int j = row; j < row + ElemPntCntH; j++)
				{
					pointStatus[i, j] = false;
				}
			}
		}

		private Point GetShiftedPosition(Vector dragOffset, Point cursePos, FrameworkElement back, FrameworkElement elem)
		{
			var newPosition = Point.Subtract(
									cursePos,
									dragOffset);
			var xMax = back.ActualWidth - elem.Width;
			var yMax = back.ActualHeight - elem.Height;
			var pos = new Point(Math.Min(Math.Max(newPosition.X, 0), xMax), Math.Min(Math.Max(newPosition.Y, 0), yMax));

			return pos;
		}

		public bool
			AddNewElement(DrawElementBase deb)
		{
			//колонка верхней левой точки
			int col = 0;
			//строчка верхней левой точки
			int row = 0;
			bool IsFinded = false;
			while (!IsFinded)
			{
				col = FindFreeColAtLine(col, row);
				IsFinded = CheckSpace(col, row);

				if (!IsFinded)
				{
					if (CheckCurRow(row))
					{
						row++;
						col = 0;
					}
					else
					{
						return false;
					}
				}
			}

			FillSpace(col, row);

			var tmpVP = new Viewport((RectWrapper)vp.World.Clone(), (RectWrapper)vp.Plane.Clone(), vp.IsConverEqual);
			tmpVP.World.X = 0;
			tmpVP.World.Y = 0;

			//находим верхнюю и нижнюю реальные точку
			Point topL = vp.ConvertToPlane(pointField[col, row]),
				botR = vp.ConvertToPlane(pointField[col + elemSegCountW, row + elemSegCountH]),
				//стандартные точки для отрисовки элемента (первый квадрат)
				inputPtr = tmpVP.ConvertToPlane(pointField[0, elemSegCountH / 2]),
				outputPtr = tmpVP.ConvertToPlane(pointField[elemSegCountW, elemSegCountH / 2]);

			//реальные точка на поле
			Point inputPtrReal = vp.ConvertToPlane(pointField[col, row + elemSegCountH / 2]),
			outputPtrReal = vp.ConvertToPlane(pointField[col + elemSegCountW, row + elemSegCountH / 2]);

			if (debug)
			{
				Shape eli = templateElp.CopyTo(new Ellipse());
				eli.SetShiftedPosition(inputPtrReal);
				SmartCanvas.Add(eli);

				eli = templateElp.CopyTo(new Ellipse());
				eli.SetShiftedPosition(outputPtrReal);
				SmartCanvas.Add(eli);

				eli = templateElp.CopyTo(new Ellipse());
				eli.Fill = Brushes.Purple;
				eli.SetShiftedPosition(topL);
				SmartCanvas.Add(eli);

				Border bor = new Border()
				{
					BorderBrush = Brushes.Orange,
					BorderThickness = new Thickness(3),
					Width = vp.ConvertToPlane(step) * elemSegCountW,
					Height = vp.ConvertToPlane(step) * elemSegCountH
				};
				bor.SetPosition(topL);
				SmartCanvas.Add(bor);

				eli = templateElp.CopyTo(new Ellipse());
				eli.Fill = Brushes.Purple;
				eli.SetShiftedPosition(vp.ConvertToPlane(pointField[col + elemSegCountW / 2, row + elemSegCountH / 2]));
				SmartCanvas.Add(eli);

				eli = templateElp.CopyTo(new Ellipse());
				eli.Fill = Brushes.Purple;
				eli.SetShiftedPosition(botR);
				SmartCanvas.Add(eli);

			}

			deb.ActualWidth = botR.X - topL.X;
			deb.ActualHeight = botR.Y - topL.Y;
			deb.ScrnInp = inputPtrReal;
			deb.ScrnOut = outputPtrReal;

			var qwe = deb.GetView(inputPtr, outputPtr, 0, -1, true);
			if (debug)
			{
				qwe.Background = Brushes.Red;
			}

			qwe.SetPosition(topL);
			SmartCanvas.Children.Add(qwe);
			Elements.Add(deb);

			NewStep?.Invoke();
			TaskStart?.Invoke();
			return true;
		}

		Point GetClearPoint(Point ptr)
		{
			return vp.ConvertToPlane(new Point(ptr.X - offsetX, ptr.Y - offsetY));
		}

		private double FindDelta()
		{
			return Math.Abs(vp.ConvertToPlane(pointField[0, 0]).X - vp.ConvertToPlane(pointField[1, 0]).X);
		}

		private bool CheckCurRow(int row)
		{
			return !(row + 1 >= pointStatus.GetLength(1));
		}

		/// <summary>
		/// True - можем вставить, False - есть пересечение
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		private bool CheckSpace(int col, int row)
		{
			if (col == -1) return false;
			if (col + elemSegCountW >= WidthPoints) return false;
			if (row + elemSegCountH >= HeightPoints) return false;
			for (int i = col; i < col + elemSegCountW + 1; i++)
			{
				for (int j = row; j < row + elemSegCountH + 1; j++)
				{
					if (pointStatus[i, j]) return false;
				}
			}
			return true;
		}

		private int FindFreeColAtLine(int col, int row)
		{
			for (int i = col; i < pointField.GetLength(0); i++)
			{
				if (pointStatus[i, row] == false) return i;
			}
			return -1;
		}

		public bool InitChec(TopicTask t)
		{
			var res = CheckTask?.Invoke(Elements,t);
			if (res.Value)
			{
				TaskEnd?.Invoke();
				return true;
			}
			return false;
		}
	}
}
