﻿using InformaticTests.GUI.ViewModels;
using InformaticTests.Logic.Moduls;
using InformaticTests.Logic.Moduls.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using ApiAkinyi;
using System.Collections.ObjectModel;

namespace InformaticTests.GUI.Views
{
	/// <summary>
	/// Interaction logic for InfoMainView.xaml
	/// </summary>
	public partial class InfoMainView : UserControl
	{

		CommandBinding LoadItem = new CommandBinding(InfoTestsViewModel.Load);
		CommandBinding Save = new CommandBinding(InfoTestsViewModel.Save);
		CommandBinding SaveAs = new CommandBinding(InfoTestsViewModel.SaveAs);
		CommandBinding ExportProg = new CommandBinding(InfoTestsViewModel.LoadProgress);	
		CommandBinding ImportProg = new CommandBinding(InfoTestsViewModel.SaveProgress);
		CommandBinding CheckTask = new CommandBinding(InfoTestsViewModel.CheckTask);

		InfoTestsViewModel viewModel;

		private void InitializeCommand()
		{
			LoadItem.Executed += LoadItem_Executed;
			Save.Executed += LoadItem_Executed;
			SaveAs.Executed += LoadItem_Executed;
			ExportProg.Executed += LoadItem_Executed;
			ImportProg.Executed += LoadItem_Executed;
			CommandBindings.Add(LoadItem);
			CommandBindings.Add(Save);
			CommandBindings.Add(SaveAs);
			CommandBindings.Add(ExportProg);
			CommandBindings.Add(ImportProg);
			CommandBindings.Add(CheckTask);
		}

		private void LoadItem_Executed(object sender, ExecutedRoutedEventArgs e)
		{

		}

		const string resourcePath = "InformaticTestsResources";

		public List<string> AllChapters = new List<string>();
		public List<string> AllTopics = new List<string>();
		public List<string> AllTasks = new List<string>();

		public InfoMainView()
		{
			InitializeComponent();
			InitializeCommand();
			viewModel = (DataContext as InfoTestsViewModel);
			recom.Add(rec);

			viewModel.Chapters = XmlInfoParser.GetTasks(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, resourcePath, "InfoTestTasks.xml"));

			TreeTasks.ItemsSource = viewModel.Chapters;

			foreach (var chap in viewModel.Chapters)
			{
				chap.ChapterTaskEnd += Chap_ChapterTaskEnd;
				if (!AllChapters.Contains(chap.Title))
					AllChapters.Add(chap.Title);
				foreach (Topic topic in chap.ChildInfo)
				{
					if (!AllTopics.Contains(topic.Title))
						AllTopics.Add(topic.Title);
					foreach (TopicTask task in topic.ChildInfo)
					{
						if (!AllTasks.Contains(task.Title))
							AllTasks.Add(task.Title);
					}
				}
			}

			Loaded += (o, a) =>
			{
				if (!Directory.Exists(resourcePath))
				{
					var asss = Assembly.GetExecutingAssembly();
					var names = asss.GetManifestResourceNames();

					for (int i = 2; i < names.Length; i++)
					{
						using (var stream = asss.GetManifestResourceStream(names[i]))
						using (var file = new FileStream(
							resourcePath + "/" + names[i].Split(new string[] { "InformaticTests.InformaticTestsResources." },
							StringSplitOptions.RemoveEmptyEntries)[0], FileMode.Create, FileAccess.Write))
						{
							stream.CopyTo(file);
						}
					}
				}				
			};
		}

		private void Chap_ChapterTaskEnd(string chap, string topic, string task, System.Diagnostics.Stopwatch span, int steps, bool done)
		{
			var data = new RawDatas();
			data.MDatas.Add(new ModulData()
			{
				MChapter = chap,
				MIsDone = done,
				MRelatedTime = span.Elapsed,
				MSteps = steps,
				MTask = task,
				MTopic = topic
			});

			ModulController.instance.PluginUpdate(data);
		}

		TextBox templateTxtBx = new TextBox()
		{
			TextWrapping = TextWrapping.Wrap,
			AcceptsReturn = true,
			IsReadOnly = true
		};

		T Clone<T>(UIElement element)
		{
			string gridXaml = XamlWriter.Save(element);
			StringReader stringReader = new StringReader(gridXaml);
			XmlReader xmlReader = XmlReader.Create(stringReader);
			return (T)XamlReader.Load(xmlReader);
		}

		private void TreeTasks_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			var elem = (sender as TreeView).SelectedItem as ITreeComp;
			TaskWindow.Children.Clear();
			TaskWindow.Children.Add(elem.TableView.GetView());			
		}

		Chapter rec = new Chapter() { Title = "Рекомендации" };
		ObservableCollection<Chapter> recom = new ObservableCollection<Chapter>();

		internal void UpdateReccomends(List<RecommendDatas.AkinyiRecomData> datas)
		{
			RecTab.IsEnabled = true;
			rec.ChildInfo.Clear();
			//recom.Clear();

			foreach (var dat in datas)
			{
				if (dat.RecomType == TypeRecom.Simular)
				{
					var chap = viewModel.Chapters.First((v) => v.Title == dat.Chapter);
					rec.ChildInfo.Add(chap);
				}
			}			

			TreeRecom.ItemsSource = recom;
			TreeRecom.DataContext = recom;
		}		
	}
}
