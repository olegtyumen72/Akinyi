﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ApiAkinyi
{
    public interface IAkinyiModule
    {		
		/// <summary>
		/// Ивент, на который подписывается рекомендательный модуль для обновления
		/// реккомендаций
		/// </summary>
		event Action<IAkinyiRawModulDatas> UpdateRecommendModule;

		/// <summary>
		/// Окно модуля
		/// </summary>
		UserControl Veiw { get; }

		/// <summary>
		/// Имя модуля, должно совпадать с название в конфиругационном файле
		/// </summary>
		string Name { get; set; }

		/// <summary>
		/// Метод для инициализации модуля
		/// </summary>
		void InitModule();

		/// <summary>
		/// Метод для подписки на обновления реккомендательного модуля
		/// </summary>
		/// <param name="rd"></param>
		void ReccomendModuleUpdated(RecommendDatas rd);

		/// <summary>
		/// Системные пользователи и данные для того, чтобы избавиться от проблемы
		/// холодного старта
		/// </summary>
		/// <returns></returns>
		IEnumerable<ColdStartUser> ColdStart();

		IEnumerable<string> AllTasks();
		IEnumerable<string> AllTopics();
		IEnumerable<string> AllChapters();

	}
}
