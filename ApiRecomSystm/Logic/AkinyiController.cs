﻿using ApiAkinyi;
using ApiRecomSystm.GUI.Windows;
using ApiRecomSystm.Logic.Moduls;
using InformaticTests.GUI.Views;
using NReco.CF.Taste.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Threading;

namespace ApiRecomSystm.Logic
{
#warning TODO: вынести инициализацию модуля в БД в модуль контроллер
	public class AkinyiController
	{

		public event Action<RecommendDatas> UpdateRecomendations;

		static AkinyiController instance;
		internal static bool CheckUser(string login, string pas)
		{
			var userId = instance.db.GetUserId(login, pas);
			if (userId == int.MinValue)
				return false;
			else
			{
				UserId = userId;
				return true;
			}
		}
		internal static bool CreateUser(string login, string pas)
		{
			var user = instance.db.CreateNewUser(login, pas);
			if (user == null) return false;
			else
			{
				UserId = (int)user[0];
				return true;
			}
		}
		public static int UserId { get; private set; }


		Dictionary<AkinyiModuleInfo, IAkinyiModule> moduls;
		ModuleController mController;
		MainWindow mainWindow;
		LoginWindow login;
		RecommendationModule recM;
		AkiniDBController db;
		AkinyiLogger akiLogger;

		public AkinyiController()
		{
			instance = this;
			akiLogger = new AkinyiLogger();
			using (db = new AkiniDBController(akiLogger.SystemId))
			{
				mainWindow = new MainWindow();

				recM = new RecommendationModule();
				mController = new ModuleController("AkiniyModuls.xml");
				moduls = mController.GetAllModules();

				login = new LoginWindow(moduls.Keys.ToList());

				if (!CheckUser("admin", "admin"))
					CreateUser("admin", "admin");

				while (login.ShowDialog() == true)
				{
					var initModls = mController.InitModuls(moduls);
					
					UpdateModulsInBD(initModls, 
						moduls.Where((v) => initModls.Any((c) => c == v.Value)).Select((v) => v.Key).ToList());

					SignModuls();

					mainWindow.AddControls(initModls.ToDictionary((v) => v.Name, (v) => v.Veiw));
					if (mainWindow.ShowDialog() == false)
					{
						break;
					}
				}

				Dispatcher.CurrentDispatcher.InvokeShutdown();
				akiLogger.SystemId = db.SystemId;
				akiLogger.SaveLog();
			}
		}

		private void UpdateModulsInBD(List<IAkinyiModule> initModls, List<AkinyiModuleInfo> modsInfo)
		{
			var dbMods = db.GetAllModuls();		

			foreach(var dbMod in dbMods)
			{
				var tmp_mdInfo = modsInfo.First((v) => v.Title == dbMod[0].ToString());
				var tmp_mdInstn = initModls.First((v) => v.Name == dbMod[0].ToString());

				if (tmp_mdInfo != null && tmp_mdInfo.Version != dbMod[1].ToString())
				{
					db.DeleteMod(dbMod);

					AddModToDB(tmp_mdInstn, tmp_mdInfo);					
				}

				modsInfo.Remove(tmp_mdInfo);
			}

			while(modsInfo.Count > 0)
			{
				var info = modsInfo.First();
				var instn = initModls.First((v) => v.Name ==  info.Title);
				AddModToDB(instn, info);

				modsInfo.Remove(info);
			}
		}

		void AddModToDB(IAkinyiModule instance, AkinyiModuleInfo info)
		{
			db.AddModul(info);
			db.Add("Chapters",  "Chapter" , instance.AllChapters());
			db.Add("Topics",  "Topic" , instance.AllTopics());
			db.Add("Tasks",  "Task" , instance.AllTasks());
			db.AddColdUsers(instance.ColdStart(), instance.Name);
		}				

		private void SignModuls()
		{
			foreach (var modul in moduls)
			{
				if (!modul.Key.Enable) continue;
				UpdateRecomendations += modul.Value.ReccomendModuleUpdated;
				modul.Value.UpdateRecommendModule += Modul_UpdateRecommendModule;
			}
		}

		private void Modul_UpdateRecommendModule(IAkinyiRawModulDatas arg)
		{
			db.AddRawData(arg);
			UpdateRecomendations?.Invoke(recM.GetReccomendations(arg.ModuleName));
		}

		public static IDataModel GetModel()
		{
			return instance.db.LoadRatingDataModel();
		}

		public static string GetChapterName(int id)
		{
			return instance.db.GetChapterName(id);
		}
	}
}
