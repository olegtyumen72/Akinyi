﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ApiRecomSystm.GUI.Windows
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		public void AddControls(Dictionary<string, UserControl> views)
		{
			foreach (var el in views)
			{
				if (el.Value == null) continue;

				TabCntr.Items.Add(new TabItem()
				{
					Content = el.Value,
					Header = el.Key,
					MaxWidth = 300
				});
			}
		}
	}
}
