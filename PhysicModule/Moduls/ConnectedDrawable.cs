﻿using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace PhysicModule.Moduls
{
	public enum PointType { Inp, Outp }

	public class ConnectedDrawable
	{

		public DrawElementBase ConObj;
		public Shape Connecter;
		public PointType ConType;

		public ConnectedDrawable()
		{


		}

	}
}
