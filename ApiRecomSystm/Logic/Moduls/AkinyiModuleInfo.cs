﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ApiRecomSystm.Logic.Moduls
{
	public class AkinyiModuleInfo : INotifyPropertyChanged
	{

		string path;
		string className;
		string title;
		bool isStatic;
		string iD;
		string version;
		bool enable;
		List<string> tags = new List<string>();
		float kofTimeSpan = 0.5F;
		float kofDone = 0.25F;
		float kofCountStep = 0.25F;


		/// <summary>
		/// Путь к файлу модуля.
		/// </summary>
		public string Path { get => path; set { path = value; OnPropertyChanged(nameof(Path)); } }

		/// <summary>
		/// Стартовый класс модуля.
		/// </summary>
		public string ClassName { get => className; set { className = value; OnPropertyChanged(nameof(ClassName)); } }

		/// <summary>
		/// Заголовок контрола модуля.
		/// </summary>
		public string Title { get => title; set { title = value; OnPropertyChanged(nameof(Title)); } }

		/// <summary>
		/// Модуль статический. сли да, то его нельзя будет выгрузить.
		/// </summary>
		public bool IsStatic { get => isStatic; set { isStatic = value; OnPropertyChanged(nameof(IsStatic)); } }

		/// <summary>
		/// ID динамического модуля
		/// </summary>
		public string ID { get => iD; set { iD = value; OnPropertyChanged(nameof(ID)); } }

		/// <summary>
		/// Версия модуля
		/// </summary>
		public string Version { get => version; set { version = value; OnPropertyChanged(nameof(Version)); } }

		/// <summary>
		/// Загружать ли плагин
		/// </summary>
		public bool Enable { get => enable; set { enable = value; OnPropertyChanged(nameof(Enable)); } }

		public List<string> Tags { get => tags; set { tags = value; OnPropertyChanged("Tags"); } }

		public void AddTag(string tag)
		{
			tags.Add(tag);
			OnPropertyChanged("Tags");
		}

		public void RemoveTag(string tag)
		{
			tags.Remove(tag);
			OnPropertyChanged("Tags");
		}

		public IEnumerable<string> GetTags
		{
			get
			{
				var res = new List<string>();

				foreach (var tag in tags)
				{
					res.Add(tag);
				}

				return res;
			}
		}

		public float KofCountSteps { get => kofCountStep; set => kofCountStep = value; }
		public float KofDone { get => kofDone; set => kofDone = value; }
		public float KofTimeSpan { get => kofTimeSpan; set => kofTimeSpan = value; }

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged(string prop = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
		}
	}
}