﻿using ApiAkinyi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml.Linq;

namespace ApiRecomSystm.Logic.Moduls
{
	public class ModuleController
	{
		public List<IAkinyiModule> allModules { get; private set; } = new List<IAkinyiModule>();
		public List<AkinyiModuleInfo> ModulsInfo { get; private set; }
		public Dictionary<string, UserControl> Controls { get; private set; } = new Dictionary<string, UserControl>();

		public Dictionary<AkinyiModuleInfo, IAkinyiModule> GetAllModules()
		{
			var res = new Dictionary<AkinyiModuleInfo, IAkinyiModule>();
			for (int i = 0; i < allModules.Count; i++)
			{
				res.Add(ModulsInfo[i], allModules[i]);
			}
			return res;
		}

		public ModuleController(string xmlPath)
		{
			//Читаем инфу о модулях
			ModulsInfo = ReadModulesInfo(xmlPath);

			foreach (var mod in ModulsInfo)
			{
				InitModule(mod
					//нужен для динамических модулей в другом домене
					//Guid.NewGuid().ToString()
					);
			}
		}

		private void InitModule(AkinyiModuleInfo mod
			//string id
			)
		{
			IAkinyiModule instance = null;

			// Если файл модуля не указан - модуль в текущем домене.
			if (mod.Path == "")
			{
				var assemblys = AppDomain.CurrentDomain.GetAssemblies();
				foreach (var asm in assemblys)
				{
					instance = asm.CreateInstance(mod.ClassName) as IAkinyiModule;
					if (instance != null) break;
				}
			}
			// если путь к фйлу указан, загрузим файл статически. 
			//Стоит ли держать и создавать модули в другом домене?
			else
			{
				if (!File.Exists(mod.Path))
					return;

				Assembly asm = Assembly.LoadFile(mod.Path);
				instance = asm.CreateInstance(mod.ClassName) as IAkinyiModule;
			}

			// Ежели тип создать не удалось, то скажем ошибка.
			if (instance == null)
			{
				throw new Exception("Can't load module " + mod.Title);
			}
			else
			{
				instance.Name = mod.Title;
				Controls.Add(mod.Title, instance.Veiw);
				allModules.Add(instance);
			}
		}

		/// <summary>
		/// Читает из файла информацию о модулях.
		/// </summary>
		/// <param name="filename">Путь к файлу с описанием модулей.</param>
		/// <returns>Информацию о модулях в виде списка.</returns>
		List<AkinyiModuleInfo> ReadModulesInfo(string filename)
		{
			var moduls = new List<AkinyiModuleInfo>();
			var doc = XDocument.Load(filename);
			var plugins = doc.Root.Elements("Module");

			foreach (var plugin in plugins)
			{
				// Путь к длл с модулем.
				string dllName = plugin.Attributes("path").Count() == 0 ? "" : plugin.Attribute("path").Value;
				string modulePath = Path.IsPathRooted(dllName) ? dllName : Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, dllName);

				// Длл статичня, то есть моно грузить в этот же домен.
				bool isStatic = plugin.Attributes("static").Count() > 0 && plugin.Attribute("static").Value == "true";

				//// Смотрим какой контрол создавать.
				//var module = plugin.Element("Module");
				//if (module == null) continue;

				var rating = plugin.Element("Ratings");

				// Параметры модуля.
				AkinyiModuleInfo info = new AkinyiModuleInfo()
				{
					Path = modulePath,
					ClassName = plugin.Attribute("class").Value,
					Title = plugin.Attribute("title") == null ? "" : plugin.Attribute("title").Value,
					Version = plugin.Attribute("version")?.Value ?? "0.0.0.0",
					Enable = bool.Parse(plugin.Attribute("enable")?.Value ?? "false"),
					KofDone = float.Parse(rating.Element("Done").Value),
					KofCountSteps = float.Parse(rating.Element("CountSteps").Value),
					KofTimeSpan = float.Parse(rating.Element("TimeSpan").Value)
				};

				var tags = plugin.Element("Tags");
				var elemsTags = tags.Value.Split(' ');
				foreach (var el in elemsTags)
				{
					info.AddTag(el);
				}

				moduls.Add(info);
			}

			return moduls;
		}

		public List<IAkinyiModule> InitModuls(Dictionary<AkinyiModuleInfo, IAkinyiModule> moduls)
		{
			var res = new List<IAkinyiModule>();

			foreach (var modul in moduls)
			{
				if (!modul.Key.Enable) continue;
				modul.Value.InitModule();
				res.Add(modul.Value);
			}

			return res;
		}
	}
}
