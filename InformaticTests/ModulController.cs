﻿using ApiAkinyi;
using InformaticTests.GUI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace InformaticTests
{
	class ModulController : IAkinyiModule
	{
		InfoMainView _view;
		string name;
		static internal ModulController instance;
		List<ColdStartUser> systUser;

		public UserControl Veiw => _view;

		public string Name { get => name; set => name = value; }

		public event Action<IAkinyiRawModulDatas> UpdateRecommendModule;

		internal void PluginUpdate(IAkinyiRawModulDatas data)
		{
			UpdateRecommendModule?.Invoke(data);
		}

		public IEnumerable<string> AllChapters()
		{
			return _view.AllChapters;
		}

		public IEnumerable<string> AllTasks()
		{
			return _view.AllTasks;
		}

		public IEnumerable<string> AllTopics()
		{
			return _view.AllTopics;
		}

		public IEnumerable<ColdStartUser> ColdStart()
		{
			return systUser;
		}

		public void InitModule()
		{
			instance = this;

			_view = new InfoMainView();

			systUser = new List<ColdStartUser>
			{
				new ColdStartUser()
				{
					Data = new List<string>() { _view.AllChapters[0], _view.AllChapters[1] },
					UsrType = CldStrUsrDataType.Chapter
				},

				new ColdStartUser()
				{
					Data = new List<string>() { _view.AllChapters[0], _view.AllChapters[1] },
					UsrType = CldStrUsrDataType.Chapter
				}
			};


			
		}

		public void ReccomendModuleUpdated(RecommendDatas rd)
		{
			if(rd.ModuleName == name)
			{
				_view.UpdateReccomends(rd.Datas);
			}
		}
	}
}
