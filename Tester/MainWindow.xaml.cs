﻿using InformaticTests.GUI.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tester
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			this.AddChild(new InfoMainView());
			//this.AddChild(new Image() {Width=100,Height=100, Source = new BitmapImage(new Uri(
			//	"/InformaticTestsResources/1", 
			//	//@"F:\Clouds\YandexDisk\Лабы курс 5\Диплом\Akinyi\Tester\bin\Debug\InformaticTestsResources\1",
			//	UriKind.Relative)) });
		}
	}
}
