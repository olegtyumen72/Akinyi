﻿using PhysicModule.Helpers;
using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PhysicModule.Models
{
	class Lightbulb : DrawElementBase
	{

		void DrawInOut(Point strIn, Point endIn, Point strOut, Point endOut)
		{
			//Create input part
			var tmpLine = new Line()
			{
				X1 = strIn.X,
				X2 = endIn.X,
				Y1 = strIn.Y,
				Y2 = endIn.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmpLine);

			var tmpElp = TemplateElp.CopyTo(new Ellipse()) as Ellipse;

			tmpElp.SetShiftedPosition(new Point(tmpLine.X1, tmpLine.Y1));
			InputUI = tmpElp;
		

			View.Children.Add(tmpElp);

			//Create outPut part
			tmpLine = new Line()
			{
				X1 = strOut.X,
				X2 = endOut.X,
				Y1 = strOut.Y,
				Y2 = endOut.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmpLine);

			tmpElp = TemplateElp.CopyTo(new Ellipse()) as Ellipse;

			tmpElp.SetShiftedPosition(new Point(tmpLine.X1, tmpLine.Y1));
			OutputUI = tmpElp;

			View.Children.Add(tmpElp);
		}		

		public override Canvas GetView(			
			Point inp,
			Point outp,
			double offset,
			double delta = -1,
			bool isRecreate = false,
			bool isVertical = false
			)
		{
			if (double.IsNaN(ActualHeight) || double.IsNaN(ActualWidth))
				throw new Exception("Width or\and Height nan");
			if (CheckElement(isRecreate, isVertical, ref inp, ref outp)) return View;

			View.Children.Clear();
			InitPoints(ref inp, ref outp, offset);
			ReSetting(delta);			

			var mainDelt = GetMainDelta();

			//считаем вспомогательный элемент
			double x2PointIn = 0, x2PointOut = 0, rangeInOut = GetRangeInOut();
			if (isVertical)
			{
				x2PointIn = inp.Y + rangeInOut * 0.25D;
				x2PointOut = outp.Y - rangeInOut * 0.25D;
			}
			else
			{
				x2PointIn = inp.X + rangeInOut * 0.25D;
				x2PointOut = outp.X - rangeInOut * 0.25D;
			}

			#region MaInputart

			Shape tmp = new Ellipse()
			{
				Width = rangeInOut * 0.5D,
				Height = rangeInOut * 0.5D,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			tmp.SetShiftedPosition(new Point(ActualWidth / 2D, ActualHeight / 2D));

			View.Children.Add(tmp);

			tmp = new Line()
			{
				X1 = inp.X + rangeInOut * 0.3D,
				X2 = outp.X - rangeInOut * 0.3D,
				Y1 = ActualHeight / 2D - rangeInOut * 0.12D,
				Y2 = ActualHeight / 2D + rangeInOut * 0.12D,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmp);

			tmp = new Line()
			{
				X1 = inp.X + rangeInOut * 0.3D,
				X2 = outp.X - rangeInOut * 0.3D,
				Y1 = ActualHeight / 2D + rangeInOut * 0.12D,
				Y2 = ActualHeight / 2D - rangeInOut * 0.12D,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmp);

			#endregion

			if (isVertical)
			{
				DrawInOut(Input, new Point(Input.X, x2PointIn), Output, new Point(Output.X, ActualHeight - x2PointIn));
			}
			else
			{
				DrawInOut(Input, new Point(x2PointIn, Input.Y), Output, new Point(ActualWidth - x2PointIn, Output.Y));
			}

			return View;
		}

	}
}
