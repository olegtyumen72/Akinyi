﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Shapes;

namespace PhysicModule.Helpers
{
	public static class Booster
	{
		public static Rect PanelToRect(this Panel value)
		{
			return new Rect(0, 0, value.ActualWidth, value.ActualHeight);
		}

		public static Point ToPoint(this Vector value)
		{
			return new Point(value.X, value.Y);
		}

		public static object FirstOrNull(this Canvas value, Func<object, bool> func)
		{
			foreach(var el in value.Children)
			{
				if (func(el)) return el;
			}

			return null;
		}



		public static int CompareTo(this Point value, Point compare)
		{
			if (value == compare)
				return 0;
			if (value.X >= compare.X && value.Y >= compare.Y)
				return 1;
			if (value.X < compare.X)
				return -1;
			if (value.Y < compare.Y)
				return -1;								

			throw new Exception("Fail compare point");
		}	

		public static Point CreateOffsetPoint(this Point value, double off)
		{			
			return new Point(value.X - off, value.Y - off);
		}

		public static void Add(this Canvas value, UIElement obj)
		{
			value.Children.Add(obj);
		}

		public static bool CompareSwapLine(this Line value1, Line value2)
		{
			if (value1.Stroke == value1.Stroke)
			{
				if (value1.X1 == value2.X2 && value1.X2 == value2.X1 && value1.Y1 == value2.Y2 && value1.Y2 == value2.Y1)
					return true;
				else
					return false;
			}
			else
				return false;
		}

		public static Point SwapXY(this Point value)
		{
			return new Point(value.Y, value.X);
		}

		public static Point GetPosition(this UIElement value)
		{
			return new Point(Canvas.GetLeft(value), Canvas.GetTop(value));
		}

		public static UIElement SetPosition(this UIElement value, Point pos)
		{
			Canvas.SetTop(value, pos.Y);
			Canvas.SetLeft(value, pos.X);
			return value;
		}

		public static Shape SetShiftedPosition(this Shape value, Point pos)
		{
			return SetShiftedPosition(value, pos.X, pos.Y);
		}

		public static Shape SetShiftedPosition(this Shape value, double x, double y)
		{
			Canvas.SetLeft(value, x - value.Width / 2D);
			Canvas.SetTop(value, y - value.Height / 2D);
			return value;				
		}

		public static Line ConnectTo(this Point value, Point dest)
		{
			return ConnectPoints(value, dest);
		}
		public static Line ConnectPoints(Point str, Point end)
		{
			return ConnectPoints(str.X, end.X, str.Y, end.Y);
		}

		public static Line ConnectPoints(double x1, double x2, double y1, double y2)
		{
			return new Line()
			{
				X1 = x1,
				X2 = x2,
				Y1 = y1,
				Y2 = y2
			};
		}

		public static Shape CopyTo(this Shape value, Shape dest)
		{
			dest.Stroke = value.Stroke;
			dest.StrokeThickness = value.StrokeThickness;
			dest.Width = value.Width;
			dest.Height = value.Height;
			dest.Fill = value.Fill;
			return dest;
		}

		public static void CreateBinding(DependencyObject obj, DependencyProperty prop, object depend, string path, BindingMode bm = BindingMode.TwoWay)
		{
			BindingOperations.SetBinding(obj, prop, CreateBind(path, depend, bm));
		}

		static Binding CreateBind(string path, object src, BindingMode bm = BindingMode.TwoWay)
		{
			return new Binding()
			{
				Source = src,
				Path = new PropertyPath(path),
				Mode = BindingMode.TwoWay,
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
			};
		}

		public static bool IsEmpty(this Point value)
		{
			return value.X == -1 || value.Y == -1;
		}
	}
}
