﻿using ApiRecomSystm.Logic.Moduls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ApiRecomSystm.GUI.Windows
{
	/// <summary>
	/// Interaction logic for LoginWindow.xaml
	/// </summary>
	public partial class LoginWindow : Window
	{
		public int UserId {get;set;}
		public bool IsNewModuls { get; private set; }

		public LoginWindow()
		{
			InitializeComponent();
		}

		public LoginWindow(List<AkinyiModuleInfo> modulsInfos)		
		{
			InitializeComponent();
			LoginDC.CreateFilteredModulTree(modulsInfos);	
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void Button_Click_1(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void TurnOnOff(object sender, RoutedEventArgs e)
		{
			if(TreeModuls.SelectedItem != null)
			{
				var tmp = TreeModuls.SelectedItem as AkinyiModuleInfo;
				tmp.Enable = !tmp.Enable;
			}
		}

		private void DeleteModule(object sender, RoutedEventArgs e)
		{

		}

		private void AddModule(object sender, RoutedEventArgs e)
		{
			var addMod = new AddModulWindow();
			if(addMod.ShowDialog() == true)
			{
				IsNewModuls = true;
			}
		}

		private void Btn_Click_ShowInfo(object sender, RoutedEventArgs e)
		{
			if(TreeModuls.SelectedItem != null)
			{
				(new AddModulWindow(TreeModuls.SelectedItem as AkinyiModuleInfo)).ShowDialog();
			}
		}
	}
}
