﻿using ApiRecomSystm.Logic.Moduls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.GUI.ModelView
{

	internal class Node
	{
		public string Name { get; set; } = "";

		public List<AkinyiModuleInfo> Mods { get; set; } = new List<AkinyiModuleInfo>();
	}

	internal class LoginWindowModelView
	{

		public List<AkinyiModuleInfo> ModulsInfos { get; set; } = new List<AkinyiModuleInfo>();

		public List<Node> Nods { get; set; } = new List<Node>();


		internal void CreateFilteredModulTree(List<AkinyiModuleInfo> moduls)
		{
			var tags = new List<string>();

			foreach(var mod in moduls)
			{
				foreach(var tag in mod.GetTags)
				{
					if (!tags.Any((v) => tag == v)) tags.Add(tag);
				}
			}


			foreach(var tag in tags)
			{
				var curNode = new Node()
				{
					Name = tag,
					Mods = moduls.Where((v) => v.Tags.Contains(tag)).ToList()
				};
				Nods.Add(curNode);				
			}

			Nods.Add(new Node() { Name = "Рекоменд. модули", Mods = moduls });
			Nods.Add(new Node() { Name = "Все", Mods = moduls });
		}

	}
}
