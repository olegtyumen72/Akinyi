﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.Logic
{
	internal static class Booster
	{

		public static void AddParameters(this SQLiteCommand cmd, IEnumerable<object> values)
		{
			foreach (var val in values)
			{
				cmd.Parameters.Add(new SQLiteParameter()
				{
					Value = val
				});
			}
		}

		public static double ConvertToDouble(this string value)
		{
			return Convert.ToDouble(value);
		}

		public static int ConvertToInt(this string value)
		{
			return Convert.ToInt32(value);
		}

		public static double ConvertToDouble(this object value)
		{
			return Convert.ToDouble(value);
		}

		public static int ConvertToInt(this object value)
		{
			return Convert.ToInt32(value);
		}

		public static TimeSpan ConvertToTime(this string value)
		{
			return TimeSpan.Parse(value);
		}

	}
}
