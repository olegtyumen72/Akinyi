﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiAkinyi
{
	public enum CldStrUsrDataType { Chapter, Topic }

	public class ColdStartUser
	{
		public CldStrUsrDataType UsrType;
		public IEnumerable<string> Data;
	}
}
