﻿using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using PhysicModule.Helpers;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Media;

namespace PhysicModule.Models
{
	class Switcher : DrawElementBase
	{

		void DrawInOut(Point strIn, Point endIn, Point strOut, Point endOut, 
			Point strSwi, Point endSwi)
		{
			var tmpLine = new Line()
			{
				X1 = strIn.X,
				X2 = endIn.X,
				Y1 = strIn.Y,
				Y2 = endIn.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmpLine);

			var tmpElip = TemplateElp.CopyTo(new Ellipse()) as Ellipse;
			tmpElip.SetShiftedPosition(tmpLine.X1, tmpLine.Y1);
			InputUI = tmpElip;

			View.Children.Add(tmpElip);

			tmpLine = new Line()
			{
				X1 = strSwi.X,
				X2 = endSwi.X,
				Y1 = strSwi.Y,
				Y2 = endSwi.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmpLine);

			tmpLine = new Line()
			{
				X1 = strOut.X,
				X2 = endOut.X,
				Y1 = strOut.Y,
				Y2 = endOut.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Children.Add(tmpLine);

			tmpElip = TemplateElp.CopyTo(new Ellipse()) as Ellipse;
			tmpElip.SetShiftedPosition(tmpLine.X1, tmpLine.Y1);
			OutputUI = tmpElip;

			View.Children.Add(tmpElip);
		}

		public override Canvas GetView(		
			Point inp, 
			Point outp,
			double offset,
			double delta = -1,
			bool isRecreate = false,
			bool isVertical = false)
		{
			if (CheckElement(isRecreate, isVertical, ref inp, ref outp)) return View;
			View.Children.Clear();
			InitPoints(ref inp ,ref outp, offset);

			ReSetting(delta);

			var mainDelt = GetMainDelta();
			var rangeInOut = GetRangeInOut();			
			
			//считаем InputPts and OutputPts
			Point endIn = new Point();
			Point endOut = new Point();

			if (isVertical)
			{
				endIn.X = Input.X;
				endIn.Y = inp.Y + rangeInOut * 0.3D;
				endOut.X = Output.X;
				endOut.Y = outp.Y - rangeInOut * 0.3D;
			}
			else
			{
				endIn.X = inp.X + rangeInOut*0.3D;
				endIn.Y = Input.Y;
				endOut.X = outp.X - rangeInOut*0.3D;
				endOut.Y = Output.Y;
			}		

			//считаем вспомогательный элемент
			Point strSwi = new Point();
			Point endSwi = new Point();
			if (isVertical)
			{
				strSwi.X = endIn.X;
				strSwi.Y = endIn.Y;
				endSwi.Y = endOut.Y;
				endSwi.X = endOut.X - rangeInOut * 0.2D;

			}
			else
			{
				strSwi.X = endIn.X;
				strSwi.Y = endIn.Y;
				endSwi.Y = endOut.Y - rangeInOut*0.2D;
				endSwi.X = endOut.X;
			}			

			DrawInOut(Input, endIn, Output, endOut, strSwi, endSwi);			
			
			return View;
		}
	}
}
