﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using PhysicModule.Helpers;
using PhysicModule.Models;
using PhysicModule.Moduls;

namespace PhysicModule.Interfaces
{
	public abstract class DrawElementBase
	{
		Point input = new Point(-1, -1);
		Point output = new Point(-1, -1);
		Point scrnInp = new Point(-1, -1);
		Point scrnOut = new Point(-1, -1);
		Shape inputUI;
		Shape outputUI;
		ConnectedDrawable inputUICon;
		ConnectedDrawable outputUICon;
		double actualWidth = 100;
		double actualHeight = 100;
		Canvas view = new Canvas()
		{
			ClipToBounds = false,
			Background =
				Brushes.Transparent,
				//Brushes.Red,
			Width = 100,
			Height = 100,
			IsHitTestVisible = false
			//IsManipulationEnabled = true
		};
		double lineWidth = 3D;
		Brush stroke = Brushes.LightGreen;
		double deltaX = 0;
		double deltaY = 0;
		Ellipse templateElp = new Ellipse()
		{
			Width = 12,
			Height = 12,
			Stroke = Brushes.Purple,
			StrokeThickness = 3,
			Fill = Brushes.Purple
		};
		bool isVertical = false;

		public Point Input { get => input; set => input = value; }
		public Point Output { get => output; set => output = value; }
		public Canvas View { get => view; set => view = value; }
		public double LineWidth { get => lineWidth; set => lineWidth = value; }
		public Brush Stroke { get => stroke; set => stroke = value; }
		public double DeltaX { get => deltaX; set => deltaX = value; }
		public double ActualWidth { get => actualWidth; set { actualWidth = value; view.Width = value; } }
		public double ActualHeight { get => actualHeight; set { actualHeight = value; view.Height = value; } }
		public Shape InputUI { get => inputUI; set { inputUI = value; UpdateConnect(inputUICon, value); } }
		public Shape OutputUI { get => outputUI; set { outputUI = value; UpdateConnect(outputUICon, value); } }
		public double DeltaY { get => deltaY; set => deltaY = value; }
		public Ellipse TemplateElp { get => templateElp; set => templateElp = value; }
		public bool IsVertical { get => isVertical; set => isVertical = value; }
		public Point ScrnInp { get => scrnInp; set => scrnInp = value; }
		public Point ScrnOut { get => scrnOut; set => scrnOut = value; }
		public ConnectedDrawable InputUICon { get => inputUICon; set => inputUICon = value; }
		public ConnectedDrawable OutputUICon { get => outputUICon; set => outputUICon = value; }

		void UpdateConnect(ConnectedDrawable conect, Shape newElip)
		{
			if(conect != null)
			{
				if (conect.ConType == PointType.Inp)
				{
					conect.ConObj.inputUICon.Connecter = newElip;
				}
				else
				{
					conect.ConObj.outputUICon.Connecter = newElip;
				}
			}
		}

		public DrawElementBase()
		{
			GetView(Input, Output, 0);
			//view.PreviewMouseLeftButtonDown += (v, o) =>
			//{

			//};
		}

		public abstract Canvas GetView(
			Point inp,
			Point outp,
			double offset,
			double delta = -1,
			bool isRecreate = false,
			bool isVertical = false);

		public DrawElementBase Clone()
		{
			var res = MemberwiseClone() as DrawElementBase;
			res.input = new Point(input.X, input.Y);
			res.output = new Point(output.X, output.Y);
			res.view = new Canvas() { ClipToBounds = view.ClipToBounds, Background = view.Background };
			res.stroke = res.Stroke.Clone();
			TemplateElp.CopyTo(res.TemplateElp);
			return res;
		}

		/// <summary>
		/// True - return view, False - rebuild
		/// </summary>
		/// <param name="isRecreate"></param>
		/// <param name="isVertical"></param>
		/// <returns></returns>
		protected bool CheckElement(bool isRecreate, bool isVertical, ref Point inp, ref Point outp)
		{
			if (IsVertical != IsVertical)
			{
				IsVertical = IsVertical;
				return false;
			}

			if (View.Children.Count > 0 && !isRecreate) return true;
			else if (inp.IsEmpty() || outp.IsEmpty())
			{
				if (!isVertical)
				{
					inp = new Point(ActualWidth * 0.1D, ActualHeight / 2D);
					outp = new Point(ActualWidth - inp.X, inp.Y);
				}
				else
				{
					inp = new Point(ActualWidth / 2D, ActualHeight * 0.1D);
					outp = new Point(ActualWidth / 2D, ActualHeight - inp.Y);
				}
				return false;
			}
			else
			{
				if (isRecreate) return false;
				else return true;
			}
		}

		protected void ReSetting(double delta)
		{
			//var rangeX = delta == -1 ? Math.Abs(Input.X + Output.X) : Math.Abs(Input.X - Output.X) + 2 * delta;
			//var rangeY = delta == -1 ? Math.Abs(Input.Y + Output.Y) : Math.Abs(Input.Y - Output.Y) + 2 * delta;

			//var maxRange = isVertical ? rangeY : rangeX;
			//ActualHeight = maxRange;
			//ActualWidth = maxRange;

			DeltaX = !isVertical ? ActualWidth * 0.1D : Input.X;
			DeltaY = isVertical ? ActualHeight * 0.1D : Input.Y;

			if (delta != -1)
			{
				if (isVertical) DeltaY = delta;
				else DeltaX = delta;
			}
		}

		protected double GetMainDelta()
		{
			return IsVertical ? DeltaY : DeltaX;
		}

		protected void InitPoints(ref Point inp, ref Point outp, double offset = 0)
		{
			inp = inp.CreateOffsetPoint(offset);
			outp = outp.CreateOffsetPoint(offset);
			Input = inp;
			Output = outp;
		}

		protected double GetRangeInOut()
		{
			if (isVertical)
			{
				return Math.Abs(input.Y - output.Y);
			}
			else
			{
				return Math.Abs(input.X - output.X);
			}
		}

		public bool HasPoint(Shape elem)
		{
			if (elem == inputUI) return true;
			if (elem == outputUI) return true;

			return false;
		}
	}
}
