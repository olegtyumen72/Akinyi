﻿using ApiAkinyi;
using PhysicModule.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PhysicModule
{
	class MainModule : IAkinyiModule
	{
		static internal MainModule instance;
		public Physic view;
		public string Name = "Электрические схемы";
		List<ColdStartUser> systUser;

		public UserControl Veiw => view;

		string IAkinyiModule.Name { get => throw new NotImplementedException(); set => Name = value; }

		public event Action<IAkinyiRawModulDatas> UpdateRecommendModule;

		internal void PluginUpdate(IAkinyiRawModulDatas data)
		{
			UpdateRecommendModule?.Invoke(data);
		}

		public IEnumerable<string> AllChapters()
		{
			return view.AllChapters;
		}

		public IEnumerable<string> AllTasks()
		{
			return view.AllTasks;
		}

		public IEnumerable<string> AllTopics()
		{
			return view.AllTopic;
		}

		public IEnumerable<ColdStartUser> ColdStart()
		{
			return systUser;
		}

		public void InitModule()
		{
			instance = this;
			view = new Physic();

			systUser = new List<ColdStartUser>
			{
				new ColdStartUser()
				{
					Data = new List<string>() { view.AllChapters[0], view.AllChapters[1] },
					UsrType = CldStrUsrDataType.Chapter
				},

				new ColdStartUser()
				{
					Data = new List<string>() { view.AllChapters[0], view.AllChapters[1] },
					UsrType = CldStrUsrDataType.Chapter
				}
			};
		}

		public void ReccomendModuleUpdated(RecommendDatas rd)
		{
			if(rd.ModuleName == Name)
			view.SetRecommendations(rd.Datas);
		}
	}
}
