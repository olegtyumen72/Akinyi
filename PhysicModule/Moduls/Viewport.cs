﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using PhysicModule.Interfaces;

namespace PhysicModule.Moduls
{

	public class Viewport
	{
		//Title
		//Коэффициенты считаются для прямого перевода из мировых координат в экранные (панель)

		IRectangle world;
		IRectangle plane;
		/// <summary>
		/// 0 - kx, 1 - ky
		/// </summary>
		double[] k;
		double xOffset = 0;
		double yOffset = 0;
		bool isConverEqual = false;

		public IRectangle World { get => world; set { world = value; InitPort(); } }
		public IRectangle Plane { get => plane; set { world = value; InitPort(); } }
		public double XOffset { get => xOffset; set { xOffset = value; InitPort(); } }
		public double YOffset { get => yOffset; set { yOffset = value; InitPort(); } }
		public bool IsConverEqual { get => isConverEqual; set { isConverEqual = value; InitPort(); } }

		public Viewport(IRectangle world, IRectangle pane,
			bool isEqual = false,
			double xOff = 0, double yOff = 0)
		{
			this.world = world;
			this.plane = pane;
			//XOffset = xOff;
			//YOffset = yOff;
			isConverEqual = isEqual;
			InitPort();

			World.Updated += () => InitPort();
			Plane.Updated += () => InitPort();
		}


		public static Viewport Clone(Viewport another)
		{			
			return new Viewport(another.world, another.plane, another.isConverEqual, another.xOffset, another.yOffset);
		}

		private void InitPort()
		{
			if (world == null || plane == null) throw new Exception();

			k = CalcKof();

		}

		private double[] CalcKof()
		{
			k = new double[2];
			k[0] = (plane.Width - xOffset) / world.Width;
			k[1] = (plane.Height - yOffset) / world.Height;

			if (IsConverEqual)
			{
				k[0] = Math.Min(k[0], k[1]);
				k[1] = Math.Min(k[0], k[1]);
			}

			return k;
		}

		public Point ConvertToWorld(Point p)
		{
			return ConvertToWorld(p.X, p.Y);
		}

		public Point ConvertToWorld(double x, double y)
		{
			return new Point(
				ConvertToWorld(world.X, plane.X, xOffset, x, k[0]),
				ConvertToWorld(world.Y, plane.Y, yOffset, y, k[1])
				);
		}

		double ConvertToWorld(double worldMin, double paneMin, double offset, double value, double k)
		{
			return worldMin +  (-offset / 2D + value - paneMin) / k;
		}

		public double ConvertToWorld(double value, bool isX = true)
		{
			if (isX)
			{
				return k[0] / value;
			}
			else
			{
				return k[1] / value;
			}
		}

		public Point ConvertToPlane(Point p)
		{
			return ConvertToPlane(p.X, p.Y);
		}

		public Point ConvertToPlane(double x, double y)
		{
			return new Point(
				ConvertToPlane(world.X, plane.X, xOffset, x, k[0]),
				ConvertToPlane(world.Y, plane.Y, yOffset, y, k[1])
				);
		}

		double ConvertToPlane(double worldMin, double paneMin, double offset, double value, double k)
		{
			return offset / 2D + paneMin + k * (value - worldMin);
		}

		public double ConvertToPlane(double value, bool isX = true)
		{
			if (isX)
			{
				return k[0] * value;
			}
			else
			{
				return k[1] * value;
			}
		}
	}
}
