﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicModule.View
{
	/// <summary>
	/// Interaction logic for Switcher.xaml
	/// </summary>
	public partial class Switcher : UserControl
	{
		double lineThick = //10; 
		3.5;
		Brush stroke = //Brushes.Red; 
		Brushes.LightGray;
		double delta = 0;

		public Switcher()
		{
			InitializeComponent();


			Loaded += (v, v1) =>
			{
				delta = ActualWidth * 0.1;
		
				Shape tmp = new Line()
				{
					X1 = delta,
					X2 = ActualWidth/2.5D,
					Y1 = ActualHeight/2D,
					Y2 = ActualHeight/2D,
					Stroke = stroke,
					StrokeThickness= lineThick
				};
			
				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = ActualWidth - ActualWidth / 2.5D ,
					X2 = ActualWidth - delta,
					Y1 = ActualHeight / 2D,
					Y2 = ActualHeight / 2D,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = ActualWidth / 2.5D,
					X2 = ActualWidth - ActualWidth / 2.5D,
					Y1 = ActualHeight / 3D,
					Y2 = ActualHeight / 2D,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);


				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, delta - tmp.ActualWidth/2D);
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height / 2F);

				DrawPanel.Children.Add(tmp);

				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, ActualWidth - delta - tmp.ActualWidth / 2D);
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height / 2F);

				DrawPanel.Children.Add(tmp);
			};
		}
	}
}
