﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiAkinyi
{

	public interface IAkinyiRawModulDatas
	{
		/// <summary>
		/// Имя модуля
		/// </summary>
		string ModuleName { get; }

		IEnumerable<IAkinyiModuleData> Datas { get; }
		
	}

	public interface IAkinyiModuleData
	{
		/// <summary>
		/// Название главы. Не пустое
		/// </summary>
		string Chapter { get; }
		/// <summary>
		/// Название темы. Может быть пустым
		/// </summary>
		string Topic { get; }
		/// <summary>
		/// Название задания. Может быть пустым
		/// </summary>
		string Task { get; }
		/// <summary>
		/// Потраченное время
		/// </summary>
		TimeSpan RelatedTime { get; }
		/// <summary>
		/// Потраченное количество действий
		/// </summary>
		int Steps { get; }
		/// <summary>
		/// Закончено ли?
		/// </summary>
		bool IsDone { get; }
	}
}
