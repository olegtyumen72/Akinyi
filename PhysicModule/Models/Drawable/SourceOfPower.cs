﻿using PhysicModule.Helpers;
using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace PhysicModule.Models
{
	class SourceOfPower : DrawElementBase
	{		

		void DrawInOut(Point In, Point Out, Point InStr, Point InEnd, Point OutStr, Point OutEnd)
		{
			Line tmpLine = new Line()
			{
				Stroke = Stroke,
				X1 = In.X,
				Y1 = In.Y,
				X2 = Out.X,
				Y2 = Out.Y,
				StrokeThickness = LineWidth
			};

			View.Add(tmpLine);

			var tmpElispe = TemplateElp.CopyTo(new Ellipse()) as Ellipse;
			tmpElispe.SetShiftedPosition(tmpLine.X1, tmpLine.Y1);
			InputUI = tmpElispe;
			View.Children.Add(tmpElispe);

			tmpElispe = TemplateElp.CopyTo(new Ellipse()) as Ellipse;
			tmpElispe.SetShiftedPosition(tmpLine.X2, tmpLine.Y2);
			OutputUI = tmpElispe;
			View.Add(tmpElispe);

			tmpLine = new Line()
			{
				X1 = InStr.X,
				Y1 = InStr.Y,
				X2 = InEnd.X,
				Y2 = InEnd.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Add(tmpLine);

			tmpLine = new Line()
			{
				X1 = OutStr.X,
				Y1 = OutStr.Y,
				X2 = OutEnd.X,
				Y2 = OutEnd.Y,
				Stroke = Stroke,
				StrokeThickness = LineWidth
			};

			View.Add(tmpLine);
		}

		public override Canvas GetView(			
			Point inp,
			Point outp,
			double offset,
			double delta = -1,
			bool isRecreate = false,
			bool isVertical = false)
		{
			if (CheckElement(isRecreate, isVertical, ref inp, ref outp)) return View;

			View.Children.Clear();
			InitPoints(ref inp,ref outp, offset);
			ReSetting(delta);

			
			var mainDelt = GetMainDelta();
			var rangeInOur = GetRangeInOut();
			var deltaL = rangeInOur * 0.15D; 
			var center = new Point(ActualWidth / 2D , ActualHeight / 2D);

			var inpStr = new Point(center.X - deltaL, center.Y - deltaL*2);
			var inpEnd = new Point(center.X - deltaL, center.Y + deltaL*2);
			var outStr = new Point(center.X + deltaL, center.Y - deltaL);
			var outEnd = new Point(center.X + deltaL, center.Y + deltaL);

			if (isVertical)
			{
				DrawInOut(inp, outp, inpStr.SwapXY(), inpEnd.SwapXY(), outStr.SwapXY(), outEnd.SwapXY());
			}
			else
			{
				DrawInOut(inp, outp, inpStr, inpEnd, outStr, outEnd);
			}

			return View;
		}
	}
}
