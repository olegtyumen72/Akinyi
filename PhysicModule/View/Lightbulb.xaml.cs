﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicModule.View
{
	/// <summary>
	/// Interaction logic for Lightbulb.xaml
	/// </summary>
	public partial class Lightbulb : UserControl
	{

		double lineThick = //10; 
		3.5;
		Brush stroke = //Brushes.Red; 
		Brushes.LightGray;
		double delta;

		public Lightbulb()
		{
			InitializeComponent();

			

			Loaded += (v, v1) =>
			{
				delta = ActualWidth * 0.1;

				Shape tmp = new Ellipse()
				{
					Width = ActualWidth*0.6D,
					Height = ActualWidth*0.6D,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				Canvas.SetTop(tmp, ActualHeight*0.4 / 2D);
				Canvas.SetLeft(tmp, ActualWidth*0.4 / 2D);

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = ActualWidth*0.3D,
					X2 = ActualWidth*0.7D,
					Y1 = ActualHeight/2D - delta*2,
					Y2 = ActualHeight/2D + delta*2,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = ActualWidth * 0.3D,
					X2 = ActualWidth * 0.7D,
					Y1 = ActualHeight / 2D + delta * 2,
					Y2 = ActualHeight / 2D - delta * 2,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = 5,
					X2 = ActualWidth*0.4D/2D,
					Y1 = ActualHeight / 2D,
					Y2 = ActualHeight / 2D,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					X1 = ActualWidth - 5,
					X2 = ActualWidth*0.8D,
					Y1 = ActualHeight / 2D,
					Y2 = ActualHeight / 2D,
					Stroke = stroke,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, 5 - tmp.Width/2D);
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height / 2D);

				DrawPanel.Children.Add(tmp);

				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, (ActualWidth - 5) - tmp.Width/2D );
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height / 2F);

				DrawPanel.Children.Add(tmp);

			};
		}
	}
}
