﻿using PhysicModule.Helpers;
using PhysicModule.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicModule.View
{
	/// <summary>
	/// Interaction logic for FieldScheme.xaml
	/// </summary>
	public partial class FieldScheme : UserControl
	{

		public FieldScheme()
		{
			InitializeComponent();

			var doc = new DockPanel()
			{
				ClipToBounds = true,
				//	Background = Brushes.Green
			};

			AddChild(doc);
			Loaded += (v, a) => mLogic.InitControl(doc);
		}

	}
}
