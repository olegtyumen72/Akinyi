﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicModule.Interfaces
{
	public interface IRectangle : ICloneable
	{
		double Width { get; set; }
		double Height { get; set; }
		double X { get; set; }			
		double Y { get; set; }

		event Action Updated;
	}
}
