SELECT Login, Chapter, Topic, Task, TimeTask, CountSteps, Done, Rating, TypeD FROM UserToModule 
LEFT JOIN Users ON UserToModule.UserId = Users.UserId 
LEFT JOIN Chapters ON Chapters.ChapterId = UserToModule.ChapterId
LEFT JOIN Topics ON Topics.TopicId = UserToModule.TopicId
LEFT JOIN Tasks ON Tasks.TaskId = UserToModule.TaskId
LEFT JOIN Statistics ON Statistics.UtoMId = UserToModule.UtoMId
LEFT JOIN Ratings ON Ratings.UtoMId = UserToModule.UtoMId