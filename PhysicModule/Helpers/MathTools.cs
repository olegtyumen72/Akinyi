﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PhysicModule.Helpers
{
	public class MathTools
	{
		public static double PointsRange(double x1, double y1, double x2, double y2)
		{
			return Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
		}

		public static double PointsRange(Point pt1, Point pt2)
		{
			return PointsRange(pt1.X, pt1.Y, pt2.X, pt2.Y);
		}

	}
}
