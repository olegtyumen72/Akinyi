﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PhysicModule.View
{
	/// <summary>
	/// Interaction logic for SourceOfPower.xaml
	/// </summary>
	public partial class SourceOfPower : UserControl
	{

		double lineThick = 3.5;
		Brush stroke = Brushes.LightGray;
		int delta = 35;

		public SourceOfPower()
		{
			InitializeComponent();			

			Loaded += (v, v1) =>
			{
				Shape tmp = new Line()
				{
					Stroke = stroke,
					X1 = delta,
					Y1 = ActualHeight / 3D,
					X2 = delta,
					Y2 = ActualHeight / 3D * 2,
					StrokeThickness = lineThick
				};		

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{
					Stroke = stroke,
					X1 = ActualWidth - delta,
					Y1 = ActualHeight / 4D,
					X2 = ActualWidth - delta,
					Y2 = ActualHeight / 4D * 3,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Line()
				{					
					Stroke = stroke,
					X1 = 5,
					Y1 = ActualHeight / 3D + (ActualHeight / 3D) / 2D,
					X2 = ActualWidth - 5,
					Y2 = ActualHeight / 3D + (ActualHeight / 3D) / 2D,
					StrokeThickness = lineThick
				};

				DrawPanel.Children.Add(tmp);

				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, 5);
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height/2F);

				DrawPanel.Children.Add(tmp);

				tmp = new Ellipse()
				{
					Width = 10,
					Height = 10,
					Stroke = Brushes.Purple,
					StrokeThickness = 5,
				};

				Canvas.SetLeft(tmp, ActualWidth - 10);
				Canvas.SetTop(tmp, ActualHeight / 2D - tmp.Height / 2F);

				DrawPanel.Children.Add(tmp);

			};
		}
	}
}
