﻿using InformaticTests.Logic.Moduls.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace InformaticTests.Logic.Moduls
{
	class XmlInfoParser
	{

		enum XmlNods { Chapter , Lection , Topic , Text , Pic , Example , AnswerSteps , Task}
		enum NodAtr { name , isRight , IsText , countChoose }

		static string parseFile;
		static int counter = 0;

		public static new ObservableCollection<Chapter> GetTasks(string file)
		{
			parseFile = file;
			counter = 0;
			var doc = XDocument.Load(file);
			var chapters = doc.Root.Elements();
			var res = new List<ITreeComp>();

			ReadChaptersTopics(chapters, null, res);

			var obs = new ObservableCollection<Chapter>();

			foreach (var el in res)
			{
				obs.Add(el as Chapter);
			}

			return obs;

		}

		static void ReadChaptersTopics(IEnumerable<XElement> elements, Branch root = null, List<ITreeComp> res = null)
		{
			foreach (var child in elements)
			{
				if (child.Name == "Lection")
				{
					root.TableView = new TitleElement()
					{
						Title = XElemToKnowElems(child.Elements())
					};
				}
				if (child.Name == "Topic")
				{
					var curElem = new Topic()
					{
						Title = child.Attribute("name").Value
					};
					root.ChildInfo.Add(curElem);
					ReadChaptersTopics(child.Elements(), curElem as Branch);
				}
				if (child.Name == "Chapter")
				{
					Chapter chap = new Chapter()
					{
						Title = child.Attribute("name").Value
					};
					res.Add(chap);
					ReadChaptersTopics(child.Elements(), chap);
				}
				if (child.Name == "Task" || child.Name == "Example")
				{
					ReadTaskExample(root, child);
				}
			}
		}


		static List<IKnowElement> XElemToKnowElems(IEnumerable<XElement> elements)
		{
			var res = new List<IKnowElement>();

			foreach (var xEl in elements)
			{
				if (xEl.Name == "Text")
				{
					res.Add(new KnowString() { Element = xEl.Value });
				}
				if (xEl.Name == "Pic")
				{
					res.Add(new KnowBitmap() { Element = xEl.Value });
				}
			}

			return res;
		}


		static void ReadTaskExample(Branch root, XElement curXElem)
		{

			var curTask = new TopicTask();
			root.ChildInfo.Add(curTask);
			curTask.Title = curXElem.Attribute("name").Value;
			var title = new List<IKnowElement>() { new KnowString() { Element = curXElem.Element("Title").Value } };
			
			if(curXElem.Name == XmlNods.Example.ToString())
			{
				curTask.TableView = new ExampleElement()
				{
					StepsSolution = XElemToKnowElems(curXElem.Element(XmlNods.AnswerSteps.ToString()).Elements()),
					Title = title
				};				
			}
			else
			{
				title.AddRange(XElemToKnowElems(curXElem.Elements()));

				curTask.TableView = new TaskElement()
				{
					Title = title,
					CountChoose = int.Parse((curXElem.Attribute(NodAtr.countChoose.ToString())?.Value ?? "0"))
				};

				ParseAnswers(curXElem.Elements("Answer"), curTask.TableView as TaskElement);
			}				
		}

		private static void ParseAnswers(IEnumerable<XElement> xmlAnswers, TaskElement taskElement)
		{
			foreach (var xmlAnswer in xmlAnswers)
			{

				if (xmlAnswer.Attribute("isText")?.Value.Length > 0)
				{
					taskElement.IsTextInput = true;
				}
				else
				{
					taskElement.Answers.Add(xmlAnswer.Value);

				}

				if (xmlAnswer.Attribute("isRight")?.Value.Length > 0 && xmlAnswer.Attribute("isRight").Value == "True")
				{
					taskElement.RightAnswers.Add(xmlAnswer.Value);
				}
			}
		}
	}
}
