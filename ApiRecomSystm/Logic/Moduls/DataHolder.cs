﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.Logic.Moduls
{
	class DataHolder
	{
		public TimeSpan ts;
		public int steps;
		public int Doned;
		int all = 0;
		public int MtoId;

		public double DoneProc { get { return Doned / all; } }

		internal void AddStats(TimeSpan curTime, int curSteps, bool curDone)
		{
			ts = ts.Add(curTime);
			steps += curSteps;
			Doned += curDone ? 1 : 0;
			all++;
		}
	}


	class DataHolder2
	{
		public List<float> ts = new List<float>();
		public List<int> steps = new List<int>();
		public List<bool> Doned = new List<bool>();
		int all = 0;
		public int MtoId;

		internal void AddStats(float curTime, int curSteps, bool curDone)
		{
			ts.Add((float)curTime);
			steps.Add(curSteps);
			Doned.Add(curDone);
			all++;
		}
	}


	class UserData
	{
		public int UserID = -1;
		public Dictionary<int, DataHolder2> topics = new Dictionary<int, DataHolder2>();
		public Dictionary<int, DataHolder2> chapters = new Dictionary<int, DataHolder2>();
		public Dictionary<int, DataHolder2> tasks = new Dictionary<int, DataHolder2>();

	}
}
