﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.Logic.Moduls
{
	public class UserData<T>
	{
		public List<T> Data;
		public int UserId;
	}

	public enum RatingSystemType { Task, Chapter, Topic }

	internal static class RatingSystem
	{			
		
		public static int GetNumberRating(Dictionary<int, List<float>> parametr, float kof)
		{
			var curUser = AkinyiController.UserId;
			var curUsrParamAver = parametr[curUser].Average();
			//parametr.Remove(curUser);
			var allAver = new List<float>();
			foreach(var paramData in parametr.Values)
			{
				allAver.Add(paramData.Average());
			}
			var maxAverParam = allAver.Max();

			return (int)((curUsrParamAver / maxAverParam) * 100 * kof);
		}

		public static int GetNonNumberStat(RatingSystemType type, List<bool> data, float kof)
		{
			switch (type)
			{
				case RatingSystemType.Task:
					{
						var aver = Math.Round(data.ConvertAll((v) => v ? 1 : 0).Average());
						return (int)(aver * 100 * kof);
					}
				default:
				case RatingSystemType.Chapter:
				case RatingSystemType.Topic:
					{
						return 50;					
					}
			}
		}

		public static int GetRating(double isDoned, TimeSpan milsec, int steps, RatingSystemType rst)
		{			
			if(rst == RatingSystemType.Task)
			{				
				return 30 + (isDoned == 100 ? 70 : 0);
			}
			if (rst == RatingSystemType.Topic)
			{
				var timeRat = (int)((milsec.TotalMinutes / 60) >= 1 ? 1 : (milsec.TotalMinutes / 60)) * 25;
				var stepRat = ((steps / 300) >= 1 ? 1 : (steps / 300)) * 25;
				var doneRat = (int)isDoned * 50;
				return timeRat + stepRat + doneRat;
			}
			if (rst == RatingSystemType.Chapter)
			{
				var timeRat = (int)((milsec.TotalMinutes / 90) >= 1 ? 1 : (milsec.TotalMinutes / 90)) * 25;
				var stepRat = ((steps / 600) >= 1 ? 1 : (steps / 600)) * 25;
				var doneRat = (int)isDoned * 50;
				return timeRat + stepRat + doneRat;
			}

			throw new Exception("Rating mistake");
		}

		internal static int GetRating(DataHolder value, RatingSystemType rts)
		{
			return GetRating(value.DoneProc, value.ts, value.steps, rts);
		}
	}
}
