﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InformaticTests.Logic.Moduls
{

	public abstract class TableElement
	{
		protected enum NamedElements { InfoTxtBlck };

		/// <summary>
		/// Постановка задачи
		/// </summary>
		public List<IKnowElement> Title { get; set; } = new List<IKnowElement>();

		#region Templates

		protected TextBlock templateTxtBlock = new TextBlock()
		{
			TextWrapping = TextWrapping.Wrap,
			Padding = new Thickness(3)
		};
		protected Border bordTempl = new Border()
		{
			BorderBrush = Brushes.Black,
			BorderThickness = new Thickness(1),
			Margin = new Thickness(3)
		};
		protected CheckBox checkTmp = new CheckBox()
		{
			Margin = new Thickness(3),

		};
		protected RadioButton radioTmp = new RadioButton()
		{
			Margin = new Thickness(3),

		};

		#endregion

		public virtual ScrollViewer GetView()
		{
			var scroll = new ScrollViewer();
			var stack = new StackPanel();
			scroll.Content = stack;

			if (Title.Count > 0)
			{

				var txtBx = Booster.Clone(templateTxtBlock);
				txtBx.Text = "Информация:";
				txtBx.Name = NamedElements.InfoTxtBlck.ToString();
				txtBx.Tag = NamedElements.InfoTxtBlck.ToString();
				BindElement(txtBx);
				stack.Children.Add(txtBx);

				var borderTitle = Booster.Clone<Border>(bordTempl);
				var stackTitle = new StackPanel();
				borderTitle.Child = stackTitle;

				foreach (var el in Title)
				{
					var view = el.GetView();
					BindElement(view);
					stackTitle.Children.Add(view);
				}

				stack.Children.Add(borderTitle);
			}

			return scroll;
		}

		protected void BindElement(params UIElement[] objs)
		{
			foreach (var el in objs)
			{
				el.SetDock(Dock.Top);
			}
		}
	}

	public class TitleElement : TableElement
	{

	}

	public class ExampleElement : TableElement
	{
		/// <summary>
		/// Описывает решение задачи
		/// </summary>
		public List<IKnowElement> StepsSolution { get; set; } = new List<IKnowElement>();

		public override ScrollViewer GetView()
		{
			var scroll = base.GetView();
			var stack = scroll.Content as StackPanel;

			//(stack.FindName(NamedElements.InfoTxtBlck.ToString()) as TextBlock).Text = "Задача:";
			stack.Children.OfType<TextBlock>().FirstOrDefault().Text = "Задача:";


			var txtBx = Booster.Clone(templateTxtBlock);
			txtBx.Text = "Решение:";
			BindElement(txtBx);
			stack.Children.Add(txtBx);

			var borderSolut = Booster.Clone(bordTempl);
			var stackSolut = new StackPanel();
			borderSolut.Child = stackSolut;
			stack.Children.Add(borderSolut);

			foreach (var step in StepsSolution)
			{
				var view = step.GetView();
				BindElement(view);
				stackSolut.Children.Add(view);
			}

			return scroll;
		}
	}

	public class TaskElement : TableElement
	{

		public event Action<TaskElement> TaskEnd;
		public event Action TaskStart;

		/// <summary>
		/// Сколько надо выбрать ответов
		/// </summary>
		public int CountChoose { get; set; } = 1;
		/// <summary>
		/// Ответы
		/// </summary>
		public List<string> Answers { get; set; } = new List<string>();
		/// <summary>
		/// Правильные ответы
		/// </summary>
		public List<string> RightAnswers { get; set; } = new List<string>();
		/// <summary>
		/// Правильно ли решил пользователь
		/// </summary>
		public bool IsRight { get; set; }
		/// <summary>
		/// Нужен ли текстовый ввод
		/// </summary>
		public bool IsTextInput { get; internal set; } = false;
		/// <summary>
		/// Количество выбраных ответов пользователем
		/// </summary>
		int countChoosen = 0;

		public int GetChoosen()
		{
			return countChoosen;
		}

		#region System elements

		Grid g;
		List<object> checkers;
		List<Rectangle> rowColBack;
		ContentControl Choosen;

		#endregion

		public override ScrollViewer GetView()
		{
			IsRight = false;
			countChoosen = 0;

			var scroll = base.GetView();
			var stack = scroll.Content as StackPanel;

			//(stack.FindName(NamedElements.InfoTxtBlck.ToString()) as TextBox).Text = "Задача:";
			stack.Children.OfType<TextBlock>().FirstOrDefault().Text = "Задача:";

			var dock = new DockPanel();
			dock.SetDock(Dock.Top);

			var txtBLock = Booster.Clone<TextBlock>(templateTxtBlock);
			txtBLock.Text = "Варианты:";
			txtBLock.SetDock(Dock.Top);
			dock.Children.Add(txtBLock);

			g = new Grid();
			var border = Booster.Clone<Border>(bordTempl);
			border.Child = g;
			DockPanel.SetDock(border, Dock.Top);
			dock.Children.Add(border);
			stack.Children.Add(dock);

			checkers = new List<object>();
			rowColBack = new List<Rectangle>();
			g.RowDefinitions.Add(new RowDefinition());
			g.RowDefinitions.Add(new RowDefinition());
			g.ColumnDefinitions.Add(new ColumnDefinition());
			g.ColumnDefinitions.Add(new ColumnDefinition());

			int col = 0; int row = 0;
			foreach (var el in Answers)
			{
				var rect = new Rectangle();
				Grid.SetColumn(rect, col);
				Grid.SetRow(rect, row);
				g.Children.Add(rect);
				rowColBack.Add(rect);

				if (!IsTextInput)
				{
					var check = InitCheck();
					check.Content = el;
					Grid.SetColumn(check, col++);
					Grid.SetRow(check, row);
					g.Children.Add(check);

					check.HorizontalAlignment = HorizontalAlignment.Center;
					check.VerticalAlignment = VerticalAlignment.Center;

					(check as ToggleButton).Checked += TableElement_Checked;
					checkers.Add(check);
				}
				else
				{
					var textIn = new TextBox();
					Grid.SetColumnSpan(textIn, 2);
					g.Children.Add(textIn);
				}

				if (col >= (int)Math.Floor(Answers.Count / 2D)) { col = 0; row++; }
			}

			TaskStart?.Invoke();
			return scroll;
		}

		private void TableElement_Checked(object sender, System.Windows.RoutedEventArgs e)
		{
			ContentControl obj = sender as ContentControl;
			
			if (CountChoose == 1)
			{
				countChoosen++;
				if (RightAnswers.Any((v) => v == obj.Content as string))
				{
					var indx = checkers.IndexOf(checkers.First((v) => v == obj));

					for (int i = 0; i < checkers.Count; i++)
					{
						SetCheckerRed(i);
					}
					SetCheckerGreen(indx);
					IsRight = true;
				}
				else
				{

					var indx = checkers.IndexOf(checkers.First((v) => ((ContentControl)v)?.Content as string == RightAnswers.First()));

					for (int i = 0; i < checkers.Count; i++)
					{
						SetCheckerRed(i);
					}
					SetCheckerGreen(indx);
					IsRight = false;

				}

				TaskEnd?.Invoke(this);
			}
			else
			{
				if (RightAnswers.Any((v) => v == obj.Content as string))
				{
					if (countChoosen == 0 && !IsRight) IsRight = true;
					countChoosen++;

					var indx = checkers.IndexOf(checkers.First((v) => v == obj));
					SetCheckerGreen(indx);
				}
				else
				{
					var indexs = new List<int>();

					checkers.Where((v) => RightAnswers.Any((vv) => vv == ((ContentControl)v)?.Content as string)).ToList().ForEach((v) => indexs.Add(checkers.IndexOf((v))));

					for (int i = 0; i < checkers.Count; i++)
					{
						SetCheckerRed(i);
					}

					foreach (var el in indexs)
					{
						SetCheckerGreen(el);
					}

					IsRight = false;
					countChoosen++;
				}

				if (countChoosen == CountChoose)
				{
					if (IsRight)
					{
						var indexs = new List<int>();

						checkers.Where((v) => RightAnswers.Any((vv) => vv != ((ContentControl)v)?.Content as string)).ToList().ForEach((v) => indexs.Add(checkers.IndexOf((v))));

						foreach (var el in indexs)
						{
							SetCheckerGreen(el);
						}
					}
					else
					{
						var indexs = new List<int>();

						checkers.Where((v) => RightAnswers.Any((vv) => vv == ((ContentControl)v)?.Content as string)).ToList().ForEach((v) => indexs.Add(checkers.IndexOf((v))));

						for (int i = 0; i < checkers.Count; i++)
						{
							SetCheckerRed(i);
						}

						foreach (var el in indexs)
						{
							SetCheckerGreen(el);
						}

						IsRight = false;
					}

					TaskEnd?.Invoke(this);
				}
			}
		}

		void SetCheckerGreen(int indx)
		{
			rowColBack[indx].Fill = Brushes.Green;
			(checkers[indx] as UIElement).IsEnabled = false;
		}

		void SetCheckerRed(int indx)
		{
			rowColBack[indx].Fill = Brushes.Red;
			(checkers[indx] as UIElement).IsEnabled = false;
		}

		ContentControl InitCheck()
		{
			ContentControl checker;

			if (CountChoose > 1)
			{
				checker = Booster.Clone<CheckBox>(checkTmp);
			}
			else
			{
				checker = Booster.Clone<RadioButton>(radioTmp);
			}

			return checker;
		}

	}
}
