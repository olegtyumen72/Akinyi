﻿using InformaticTests.Logic.Moduls.Text;
using InformaticTests.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace InformaticTests.GUI.ViewModels
{
	class InfoTestsViewModel
	{	
		static RoutedUICommand SaveCmd = new RoutedUICommand("Сохранить", "Save", typeof(InfoTestsViewModel));
		static RoutedUICommand SaveAsCmd = new RoutedUICommand("Сохранить как", "SaveAs", typeof(InfoTestsViewModel));
		static RoutedUICommand LoadCmd = new RoutedUICommand("Загрузить", "Load", typeof(InfoTestsViewModel));
		static RoutedUICommand LoadProgressCmd = new RoutedUICommand("Загрузить прогресс", "LoadProg", typeof(InfoTestsViewModel));
		static RoutedUICommand SaveProgressCmd = new RoutedUICommand("Сохранить прогресс", "SaveProg", typeof(InfoTestsViewModel));
		static RoutedUICommand CheckTaskCmd = new RoutedUICommand("Проверить задачу", "CheckTask", typeof(InfoTestsViewModel));		

		public static RoutedUICommand Save
		{
			get { return SaveCmd; }
		}
		public static RoutedUICommand SaveAs
		{
			get { return SaveAsCmd; }
		}
		public static RoutedUICommand Load
		{
			get { return LoadCmd; }
		}
		public static RoutedUICommand LoadProgress
		{
			get { return LoadProgressCmd; }
		}
		public static RoutedUICommand SaveProgress
		{
			get { return SaveProgressCmd; }
		}
		public static RoutedUICommand CheckTask
		{
			get { return CheckTaskCmd; }
		}

		static InfoTestsViewModel()
		{
			//SaveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
			//SaveAsCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Shift));
		}

		public InfoTestsViewModel()
		{
			
		}


		public ObservableCollection<Chapter> Chapters;
	}
}
