﻿using PhysicModule.GUI;
using PhysicModule.Interfaces;
using PhysicModule.Models;
using PhysicModule.Models.Text;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhysicModule.ModelView
{
	public class PhysicModelView
	{
		public ObservableCollection<Chapter> Chapters { get; set; } = new ObservableCollection<Chapter>();
		public ObservableCollection<DrawElementBase> DrawElements { get; set; } = new ObservableCollection<DrawElementBase>();

		static RoutedUICommand SaveCmd = new RoutedUICommand("Сохранить", "Save", typeof(PhysicModelView));
		static RoutedUICommand SaveAsCmd = new RoutedUICommand("Сохранить как", "SaveAs", typeof(PhysicModelView));
		static RoutedUICommand LoadCmd = new RoutedUICommand("Загрузить", "Load", typeof(PhysicModelView));
		static RoutedUICommand LoadProgressCmd = new RoutedUICommand("Загрузить прогресс", "LoadProg", typeof(PhysicModelView));
		static RoutedUICommand SaveProgressCmd = new RoutedUICommand("Сохранить прогресс", "SaveProg", typeof(PhysicModelView));
		static RoutedUICommand CheckTaskCmd = new RoutedUICommand("Проверить задачу", "CheckTask", typeof(PhysicModelView));
		public static RoutedUICommand RouteElemCmd = new RoutedUICommand("Повернуть элемент", "RouteElem", typeof(PhysicModelView));

		public static RoutedUICommand Save
		{
			get { return SaveCmd; }
		}
		public static RoutedUICommand SaveAs
		{
			get { return SaveAsCmd; }
		}
		public static RoutedUICommand Load
		{
			get { return LoadCmd; }
		}
		public static RoutedUICommand LoadProgress
		{
			get { return LoadProgressCmd; }
		}
		public static RoutedUICommand SaveProgress
		{
			get { return SaveProgressCmd; }
		}
		public static RoutedUICommand CheckTask
		{
			get { return CheckTaskCmd; }
		}

		static PhysicModelView()
		{
			//SaveCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
			//SaveAsCmd.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Shift));
		}

		public PhysicModelView()
		{
			Test();
		}

		private void Test()
		{
			Chapters = new ObservableCollection<Chapter>(){
				new Chapter()
				{
					Title = "Последовательное соединение",
					ChildInfo = new SmartList()
					  {
						   new Topic()
						   {
								Title = "Простые задачи",
						   },
						   new Topic()
						   {
								Title = "Задачи среднего уровня",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   },
						   new Topic()
						   {
								Title = "Продвинутые задачи",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   }
					  }
				},
				new Chapter()
				{
					Title = "Параллельное соединение",
					ChildInfo = new SmartList()
					  {
						   new Topic()
						   {
								Title = "Простые задачи",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   },
						   new Topic()
						   {
								Title = "Средние задачи",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   },
						   new Topic()
						   {
								Title = "Сложные задачи",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   }
					  }
				},
				new Chapter()
				{
					Title = "Комбинированные электросхемы",
					ChildInfo = new SmartList()
					  {
						   new Topic()
						   {
								Title = "Topic 1",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   },
						   new Topic()
						   {
								Title = "Topic 2",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   },
						   new Topic()
						   {
								Title = "Topic 3",
								 ChildInfo = new SmartList()
								 {
									  new TopicTask()
									  {
										   Title = "Task 1"
									  },
									  new TopicTask()
									  {
										   Title = "Task 2"
									  },
									  new TopicTask()
									  {
										   Title = "Task 3"
									  },
								 }
						   }
					  }
				}
			};

			var taski = new TopicTask("Моя первая схема", (Physic v) =>
										 {
											 v.TaskText.Text = $@"Моя первая схема:
Собрать электрическую микросхему состоящую из источника питания и лампочки";
										 });
			taski.CheckTask = (List<DrawElementBase> elems, TopicTask v) =>
			{
				v.Done = true;

				return true;
			};

			(Chapters.First().ChildInfo.First() as Topic).ChildInfo.Add(taski);

			taski = new TopicTask()
			{
				Title = "Моя вторая схема"
			};
			(Chapters.First().ChildInfo.First() as Topic).ChildInfo.Add(taski);

			taski = new TopicTask()
			{
				Title = "Использование резисторов"
			};
			(Chapters.First().ChildInfo.First() as Topic).ChildInfo.Add(taski);

			DrawElements.Add(new SourceOfPower());
			DrawElements.Add(new Lightbulb());
			DrawElements.Add(new Switcher());
		}
	}
}
