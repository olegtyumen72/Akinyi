﻿using ApiAkinyi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicModule.Logic.Moduls
{
	internal class RawDatas : IAkinyiRawModulDatas
	{
		public List<ModulData> MDatas = new List<ModulData>();

		public string ModuleName => MainModule.instance.Name;

		public IEnumerable<IAkinyiModuleData> Datas => MDatas;
	}

	internal class ModulData : IAkinyiModuleData
	{
		public string MChapter;
		public string MTopic;
		public string MTask;
		public TimeSpan MRelatedTime;
		public int MSteps;
		public bool MIsDone;

		public string Chapter => MChapter;

		public string Topic => MTopic;

		public string Task => MTask;

		public TimeSpan RelatedTime => MRelatedTime;

		public int Steps => MSteps;

		public bool IsDone => MIsDone;
	}
}
