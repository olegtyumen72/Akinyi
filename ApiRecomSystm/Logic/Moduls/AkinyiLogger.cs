﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.Logic.Moduls
{
	internal class AkinyiLogger
	{
		public static AkinyiLogger Instance;

		const string logFile = "../Akinyi.ini";

		public int SystemId { get; set; } = -1;

		public AkinyiLogger()
		{
			if (!File.Exists(logFile))
				using (File.Create(logFile))
				{

				}

			LoadLog();
			Instance = this;
		}

		private void LoadLog()
		{
			try
			{
				var formater = new BinaryFormatter();
				var res = new List<object>();

				using (var read = File.OpenRead(logFile))
				{
					if (read.Position != read.Length)
					{
						SystemId = (int)formater.Deserialize(read);
					}
				}
			}
			catch (Exception e)
			{

			}
		}

		public void SaveLog()
		{
			try
			{
				var formater = new BinaryFormatter();

				using (var write = File.OpenWrite(logFile))
				{
					formater.Serialize(write, SystemId);
				}
			}
			catch (Exception e)
			{

			}
		}
	}
}
