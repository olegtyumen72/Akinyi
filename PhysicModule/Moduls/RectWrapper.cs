﻿using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SA = System.Drawing;
using SW = System.Windows;

namespace PhysicModule.Moduls
{
	class RectWrapper : IRectangle		
	{
		public event Action Updated;

		SW.Rect obj;		

		public double Width
		{
			get => obj.Width;
			set { obj.Width = value; Updated.Invoke(); }
		}
		public double Height
		{
			get => obj.Height;
			set { obj.Height = value; Updated.Invoke(); }
		}
		public double X
		{
			get => obj.X;
			set { obj.X = value; Updated.Invoke(); }
		}
		public double Y
		{
			get => obj.Y;
			set { obj.Y = value; Updated.Invoke(); }
		}

		public RectWrapper(SW.Rect rect)
		{
			obj = rect;
		}

		public object Clone()
		{
			return new RectWrapper(new SW.Rect(obj.X, obj.Y, obj.Width, obj.Height));
		}
	}
}
