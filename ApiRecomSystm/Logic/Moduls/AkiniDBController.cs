﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using ApiAkinyi;
using System.Linq;
using System.Data;
using NReco.CF.Taste.Model;
using ApiRecomSystm.Logic;

namespace ApiRecomSystm.Logic.Moduls
{
	public class AkiniDBController : IDisposable
	{
		enum Tables { Ratings, Users, UserToModule, Statistics, Moduls, Chapters, Topics, Tasks }

		List<string> GetCols(Tables tab)
		{
			switch (tab)
			{
				case Tables.UserToModule:
					{
						return new List<string>() { "UtoMid", "UserId", "ModulId", "ChapterId", "TopicId", "TaskId" };
					}
				case Tables.Users:
					{
						return new List<string>() { "UserId", "Login", "Pas" };
					}
				case Tables.Ratings:
					{
						return new List<string>() { "UtoMId", "Rating", "TypeD" };
					}
				default:
					{
						return new List<string>();
					}
			}
		}

		SQLiteConnection con;
		const string db = "Akinyi.db";
		const string script = "dbScript.sql";
		public bool IsFresh = false;
		const int NullValue = int.MinValue;

		#region debug

		const bool isClearStart = false;

		#endregion

		public AkiniDBController(int freeSystemId)
		{
			SystemId = freeSystemId;

			if (isClearStart && File.Exists(db))
				File.Delete(db);

			if (!File.Exists(db))
			{
				SQLiteConnection.CreateFile(db);
				SystemId = -1;
				IsFresh = true;
			}

			con = new SQLiteConnection($"Data Source={db}");
			{
				con.Open();
				if (IsFresh)
				{
					RecreateDBStruct();
				}
				if (freeSystemId == -1 && !IsFresh)
				{
					IsFresh = true;
					RecreateDBStruct();
				}
			}
		}

		void Add(Tables table, List<string> col, IEnumerable<object> values)
		{
			var sql = $"INSERT INTO {table.ToString()}({String.Join(",", col)}) VALUES({String.Join(",", col.Select((v) => "?"))})";

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				cmd.AddParameters(values);
				//AddCmdParameters(cmd, values);
				cmd.ExecuteNonQuery();
			}
		}

		void AddCmdParameters(SQLiteCommand cmd, IEnumerable<object> values)
		{
			foreach (var val in values)
			{
				cmd.Parameters.Add(new SQLiteParameter()
				{
					Value = val
				});
			}
		}

		internal IDataModel LoadRatingDataModel()
		{
			var sql = $"SELECT UserId, ChapterId, Rating FROM USERTOMODULE LEFT OUTER JOIN RATINGS ON USERTOMODULE.UtoMId = RATINGS.UtoMId WHERE TypeD = 2";
			IDataModel dataModel = null;


			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				var dataModelLoader = new DataModelSqlLoader(cmd, "UserId", "ChapterId", "Rating");
				//var dataModelLoader = new DataModelSqlLoader(cmd, "UserId", "ChapterId");
				dataModel = dataModelLoader.Load();
				return dataModel;
			}
		}

		internal void DeleteMod(List<object> modVersion)
		{
			var modId = GetModulId(modVersion[0].ToString(), modVersion[1].ToString());
			var UToMs = ExecuteSelectQuery(Tables.UserToModule, new[] { "UtoMID" }, $"ModulId = \"{modId}\"");

			foreach (var UtoM in UToMs)
			{
				ExecuteDeleteQuery(Tables.Ratings, $"UtoMID = \"{UtoM[0]}\"");
				ExecuteDeleteQuery(Tables.Statistics, $"UtoMID = \"{UtoM[0]}\"");
			}

			ExecuteDeleteQuery(Tables.UserToModule, $"ModulId = \"{modId}\"");
			ExecuteDeleteQuery(Tables.Moduls, $"ModulId = \"{modId}\"");
		}

		private object GetModulId(string modul, string version)
		{
			return GetIdByName("Moduls", new[] { "ModulId" }, $"Module = \"{modul}\" AND Version = \"{version}\"");
		}

		internal List<List<object>> GetAllModuls()
		{
			var tmp = ExecuteSelectQuery(Tables.Moduls, new string[] { "Module", "Version" }, "");
			return tmp;
		}

		internal void Add(string table, List<string> col, List<List<string>> spisok)
		{
			var sql = $"INSERT INTO {table}({String.Join(",", col)}) VALUES({String.Join(",", col.Select((v) => "?"))})";

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				for (int globalPos = 0; globalPos < spisok[0].Count; globalPos += 1)
				{
					for (int col_i = 0; col_i < spisok.Count; col_i++)
					{
						cmd.Parameters.Add(new SQLiteParameter()
						{
							Value = spisok[col_i][globalPos],
							DbType = DbType.String
						});
					}
					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();
				}
			}
		}

		internal void Add(string table, string col, IEnumerable<string> spisok)
		{

			spisok = CheckData(table, col, spisok.ToList());
			if (spisok.Count() <= 0) return;

			var sql = $"INSERT INTO {table}({col}) VALUES(?)";

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;

				for (int col_i = 0; col_i < spisok.Count(); col_i++)
				{
					cmd.Parameters.Add(new SQLiteParameter()
					{
						Value = spisok.ElementAt(col_i),
						DbType = DbType.String
					});

					cmd.ExecuteNonQuery();
					cmd.Parameters.Clear();
				}
			}
		}

		private IEnumerable<string> CheckData(string table, string col, List<string> spisok)
		{
			var data = ExecuteSelectQuery(table, new string[] { col }, "");

			foreach (var el in data)
			{
				var elStr = el.First().ToString();
				if (spisok.Any((v) => v == elStr))
				{
					spisok.Remove(elStr);
				}
			}

			return spisok;
		}

		internal int SystemId { get; private set; } = -1;

		internal void AddColdUsers(IEnumerable<ColdStartUser> users, string modulTitle)
		{
			var lUsers = users.ToList();
			var modulId = GetModulId(modulTitle);

			using (var cmd = con.CreateCommand())
			{
				for (int i = 0; i < users.Count(); i++)
				{
					var userId = SystemId--;
					Add(Tables.Users, GetCols(Tables.Users), new object[] {
						userId,
						Guid.NewGuid().ToString().Substring(0, 4),
						Guid.NewGuid().ToString().Substring(0, 4) }
					);

					foreach (var el in lUsers[i].Data)
					{
						var uToMdId = SystemId--;
						var table = Tables.UserToModule;
						var chapterId = lUsers[i].UsrType == CldStrUsrDataType.Chapter ? GetChapterId(el) : -1;
						var topicId = lUsers[i].UsrType == CldStrUsrDataType.Topic ? GetTopicId(el) : -1;
						var taskId = -1;
						var values = new List<object>() {
							uToMdId,
							userId,
							modulId,
							chapterId,
							topicId,
							taskId
						};

						var rows = ExecuteSelectQuery(table, new string[] { "*" }, $"UserId = \"{userId}\" & ModulId = \"{modulId}\" & ChapterId = \"{chapterId}\" & topicId = \"{topicId}\" & UserId = \"{taskId}\"");

						if (rows.Count > 0) continue;

						Add(table, GetCols(table), values);

						table = Tables.Ratings;
						Add(table, GetCols(table), new object[]
						{
							uToMdId,
							100,
							ColdUserToType(lUsers[i].UsrType)
						});
					}
				}
			}
		}

		private int ColdUserToType(CldStrUsrDataType usrType)
		{
			if (usrType == CldStrUsrDataType.Chapter)
			{
				return 2;
			}
			if (usrType == CldStrUsrDataType.Topic)
			{
				return 1;
			}

			throw new Exception("Miss cold user type");
		}

		internal int GetUserId(string login, string pas)
		{
			try
			{
				return GetIdByName("Users", new[] { "UserId" }, $"Login = \"{login}\" AND Pas = \"{pas}\"");
			}
			catch (Exception e)
			{
				return NullValue;
			}
		}

		internal int GetModulId(string modul)
		{
			return GetIdByName("Moduls", new[] { "ModulId" }, $"Module = \"{modul}\"");
		}

		internal string GetChapterName(int chapterId)
		{
			return GetNameById(Tables.Chapters, new string[]
			{
				"Chapter"
			}, $"ChapterId = {chapterId}");
		}

		int GetUtoMid(int user, int modul, int chapter, int topic, int task)
		{
			try
			{
				return GetIdByName(Tables.UserToModule, new[] { "UtoMId" }, $"UserId = \"{user}\" AND ModulId = \"{modul}\" AND ChapterId = \"{chapter}\" AND TopicId = \"{topic}\" AND TaskId = \"{task}\"");
			}
			catch
			{
				return NullValue;
			}

		}

		int GetChapterId(string chapter)
		{
			return GetIdByName("Chapters", new[] { "ChapterId" }, $"Chapter = \"{chapter}\"");
		}

		int GetTopicId(string topic)
		{
			return GetIdByName("Topics", new[] { "TopicId" }, $"Topic = \"{topic}\"");
		}

		int GetTaskId(string task)
		{
			return GetIdByName("Tasks", new[] { "TaskId" }, $"Task = \"{task}\"");
		}

		string GetNameById(Tables tbl, IEnumerable<string> col, string where)
		{
			return ExecuteSelectQuery(tbl, col, where)[0][0].ToString();
		}

		int GetIdByName(string Table, IEnumerable<string> col, string where)
		{
			return int.Parse(ExecuteSelectQuery(Table, col, where).First().First().ToString());
		}

		int GetIdByName(Tables table, IEnumerable<string> col, string where)
		{
			return GetIdByName(table.ToString(), col, where);
		}

		internal object[] CreateNewUser(string login, string pas)
		{
			var res = new object[2];
			try
			{
				ExecuteStandartCmd(userCmd, new List<string> { login, pas });
				res[1] = login;
				res[0] = GetUserId(login, pas);
				return res;
			}
			catch
			{
				return null;
			}
		}

		List<List<object>> ReadExecureReader(SQLiteDataReader r)
		{
			var res = new List<List<object>>();
			while (r.Read())
			{
				res.Add(new List<object>());
				for (int i = 0; i < r.FieldCount; i++)
				{
					res.Last().Add(r[i]);
				}
			}
			return res;
		}

		List<List<object>> ExecuteSelectQuery(string Table, IEnumerable<string> cols, string Where)
		{
			var sql = $"SELECT {String.Join(",", cols)} FROM {Table}";
			if(Where.Length > 0)
			{
				sql += $" WHERE {Where}";
			}

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				var r = cmd.ExecuteReader();
				return ReadExecureReader(r);
			}
		}

		void ExecuteDeleteQuery(Tables table, string Where)
		{
			var sql = $"DELETE FROM {table} WHERE {Where}";

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				cmd.ExecuteNonQuery();
			}
		}

		List<List<object>> ExecuteSelectQuery(Tables Table, IEnumerable<string> cols, string Where)
		{
			var sql = $"SELECT {String.Join(",", cols)} FROM {Table.ToString()}";
			if (Where.Length > 0)
			{
				sql += $" WHERE {Where}";
			}
			var res = new List<List<object>>();
			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				var r = cmd.ExecuteReader();
				return ReadExecureReader(r);
			}
		}

		List<List<object>> ExecuteSelectLeftJoin(Tables tableLeft, Tables tableRight, IEnumerable<string> cols, string On, string Where)
		{
			var sql = $"SELECT {String.Join(",", cols)} FROM {tableLeft.ToString()} LEFT OUTER JOIN {tableRight.ToString()} ON {On}";
			if (Where.Length > 0)
			{
				sql += $" WHERE {Where}";
			}

			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = sql;
				var r = cmd.ExecuteReader();
				return ReadExecureReader(r);
			}
		}


		const string chapterCmd = "INSERT INTO Chapters(Chapter) VALUES(?)";
		const string topicCmd = "INSERT INTO Topics(Topic) VALUES(?)";
		const string taskCmd = "INSERT INTO Tasks(Task) VALUES(?)";
		const string ratingCmd = "INSERT INTO Ratings(UtoMId,Rating, TypeD) VALUES(?,?,?)";
		const string ratingCmd_full = "INSERT INTO Ratings(RatId,UtoMId,Rating) VALUES(?,?,?)";
		const string StatisticCmd = "INSERT INTO Statistics(UtoMId,TimeTask,CountSteps,Done) VALUES(?,?,?,?)";
		const string user_to_modul_cmd =
			"INSERT INTO UserToModule(UserId,ModulId,ChapterId,TopicId,TaskId) VALUES(?,?,?,?,?)";
		const string user_to_modul_cmd_full =
			"INSERT INTO UserToModule(UtoMId,UserId,ModulId,ChapterId,TopicId,TaskId) VALUES(?,?,?,?,?)";
		const string userCmd = "INSERT INTO Users(Login, Pas) VALUES(?,?)";
		const string userCmdFull = "INSERT INTO Users(UserId, Login, Pas) VALUES(?,?,?)";
		const string modulCmd = "INSERT INTO Moduls(Module, Version) VALUES(?,?)";

		internal void AddRawData(IAkinyiRawModulDatas value)
		{
			int modId, chapterId, topicId, taskId, userId, u_to_m_id;
			userId = AkinyiController.UserId;
			modId = GetModulId(value.ModuleName);

			foreach (var data in value.Datas)
			{
				chapterId = GetChapterId(data.Chapter);
				if (data.Topic == null)
				{
					topicId = -1;
				}
				else
				{
					topicId = data.Topic.Length > 0 ? GetTopicId(data.Topic) : -1;
				}
				if (data.Topic == null)
				{
					taskId = -1;
				}
				else
				{
					taskId = data.Task.Length > 0 ? GetTaskId(data.Task) : -1;
				}

				u_to_m_id = GetUtoMid(userId, modId, chapterId, topicId, taskId);
				if (u_to_m_id == NullValue)
				{
					ExecuteStandartCmd(user_to_modul_cmd, new object[] {
						userId,
						modId,
						chapterId,
						topicId,
						taskId
						});

					u_to_m_id = GetUtoMid(userId, modId, chapterId, topicId, taskId);
				}
				else if (taskId < 0)
				{
					ExecuteDeleteQuery(Tables.Statistics, $"UtoMId = {u_to_m_id}");
				}

				ExecuteStandartCmd(StatisticCmd, new object[]
					{
						u_to_m_id,
						data.RelatedTime.TotalMilliseconds,
						data.Steps,
						data.IsDone
					});

			}

			UpdateRating_qwe(modId);

		}

		private void UpdateRating_qwe(int modId)
		{
			var users = ExecuteSelectQuery(Tables.Users, new[] { "UserID" }, "").ConvertAll((v) => int.Parse(v.First().ToString())).Distinct();
			var usersData = new List<UserData>();

			foreach (var user in users)
			{
				if (user < 0) continue;
				var rows = ExecuteSelectLeftJoin(Tables.UserToModule, Tables.Statistics, new string[]
					{
					$"{Tables.UserToModule}.UtoMId", //0
					"ChapterId", //1
					"TopicId", //2
					"TaskId", //3
					"TimeTask", //4
					"CountSteps", //5
					"Done" //6
					}, $"{Tables.UserToModule.ToString()}.UtoMId = {Tables.Statistics.ToString()}.UtoMId", $"UserId = \"{user}\" AND ModulId = \"{modId}\"");
				var topics = new Dictionary<int, DataHolder2>();
				var chapters = new Dictionary<int, DataHolder2>();
				var tasks = new Dictionary<int, DataHolder2>();
				foreach (var row in rows)
				{
					int topicID = row[2].ToString().ConvertToInt();
					int chapterID = row[1].ToString().ConvertToInt();
					int taskID = row[3].ToString().ConvertToInt();
					if (!topics.Keys.Contains(topicID))
					{
						topics.Add(topicID, new DataHolder2());
						topics.Last().Value.MtoId = row[0].ToString().ConvertToInt();
					}
					if (!chapters.Keys.Contains(chapterID))
					{
						chapters.Add(chapterID, new DataHolder2());
						chapters.Last().Value.MtoId = row[0].ToString().ConvertToInt();
					}
					if (!tasks.Keys.Contains(taskID))
					{
						tasks.Add(taskID, new DataHolder2());
						tasks.Last().Value.MtoId = row[0].ToString().ConvertToInt();
					}

					float curTime = (float)row[4].ToString().ConvertToDouble();
					int curSteps = row[5].ToString().ConvertToInt();
					bool curDone = row[6].ToString().ConvertToInt() == 1;

					topics[topicID].AddStats(curTime, curSteps, curDone);
					chapters[chapterID].AddStats(curTime, curSteps, curDone);
					tasks[taskID].AddStats(curTime, curSteps, curDone);

					usersData.Add(new UserData()
					{
						chapters = chapters,
						topics = topics,
						tasks = tasks,
						UserID = user
					});
				}				

				//Func<int, List<float>> getterData = (v) => ExecuteSelectQuery(Tables.Statistics, new[] { "TimeTask" }, $"UtoMID = \"{v}\"").First().ConvertAll((v1) => float.Parse(v1.ToString()));
				//Func<Dictionary<int, List<float>>> getter = () =>
				// {
				//	 var data = new Dictionary<int, List<float>>();
				//	 foreach (var user in users)
				//	 {
				//		 var curData = new List<float>();
				//		 var uToMIDs = ExecuteSelectQuery(Tables.UserToModule, new[] { "UtoMID" }, $"UserID = \"{user}\" AND ModulId = \"{modId}\"").First().ConvertAll((v) => int.Parse(v.ToString())).Distinct();
				//		 foreach (var uTiMID in uToMIDs)
				//		 {
				//			 var times = getterData(uTiMID);
				//		 //ExecuteSelectQuery(Tables.Statistics, new[] { "TimeTask" }, $"UtoMID = \"{uTiMID}\"").First().ConvertAll((v) => float.Parse(v.ToString()));
				//		 curData.AddRange(times);
				//		 }
				//		 data.Add(user, curData);
				//	 };

				//	 return data;
				// };
				//var timeP = getter();
				//getterData = (v) => ExecuteSelectQuery(Tables.Statistics, new[] { "CountSteps" }, $"UtoMID = \"{v}\"").First().ConvertAll((v1) => float.Parse(v1.ToString()));
				//var stepsP = getter;

			}

			var curUser = AkinyiController.UserId;
			var curData = usersData.First((v) => v.UserID == curUser);
			foreach(var task in curData.tasks)
			{
				var qwe = usersData.Where((v) => v.tasks.Keys.Contains(task.Key)).ToList();
				var dic = qwe.ToDictionary((v) => v.UserID, (v) => v.tasks[task.Key].ts);
				var TTRating = RatingSystem.GetNumberRating(dic, 0.4F);

				var dic1 = qwe.ToDictionary((v) => v.UserID, (v) => v.tasks[task.Key].steps.ConvertAll((v1) => Convert.ToSingle(v1)));
				var TSRating = RatingSystem.GetNumberRating(dic1, 0.1F);

				var dic2 = curData.tasks[task.Key].Doned;
				var TDRating = RatingSystem.GetNonNumberStat(RatingSystemType.Task, dic2, 0.5F);

				try
				{
					ExecuteDeleteQuery(Tables.Ratings, $"UtoMID = \"{task.Value.MtoId}\"");
				}
				catch(Exception e) { System.Windows.MessageBox.Show(e.Message); }


				ExecuteStandartCmd(ratingCmd, new object[] { task.Value.MtoId, TTRating + TDRating + TSRating, 0 });
			}
			foreach (var topic in curData.topics)
			{
				var qwe = usersData.Where((v) => v.topics.Keys.Contains(topic.Key)).ToList();
				var dic = qwe.ToDictionary((v) => v.UserID, (v) => v.topics[topic.Key].ts);
				var TRating = RatingSystem.GetNumberRating(dic, 0.4F);

				var dic1 = qwe.ToDictionary((v) => v.UserID, (v) => v.topics[topic.Key].steps.ConvertAll((v1) => Convert.ToSingle(v1)));
				var SRating = RatingSystem.GetNumberRating(dic1, 0.1F);

				var dic2 = curData.topics[topic.Key].Doned;
				var DRating = RatingSystem.GetNonNumberStat(RatingSystemType.Topic, dic2, 0.5F);

				try
				{
					ExecuteDeleteQuery(Tables.Ratings, $"UtoMID = \"{topic.Value.MtoId}\"");
				}
				catch { }

				ExecuteStandartCmd(ratingCmd, new object[] { topic.Value.MtoId, TRating + DRating + SRating, 1 });
			}
			foreach (var chapter in curData.chapters)
			{
				var qwe = usersData.Where((v) => v.chapters.Keys.Contains(chapter.Key)).ToList();
				var dic = qwe.ToDictionary((v) => v.UserID, (v) => v.chapters[chapter.Key].ts);
				var TRating = RatingSystem.GetNumberRating(dic, 0.4F);

				var dic1 = qwe.ToDictionary((v) => v.UserID, (v) => v.chapters[chapter.Key].steps.ConvertAll((v1) => Convert.ToSingle(v1)));
				var SRating = RatingSystem.GetNumberRating(dic1, 0.1F);

				var dic2 = curData.chapters[chapter.Key].Doned;
				var DRating = RatingSystem.GetNonNumberStat(RatingSystemType.Chapter, dic2, 0.5F);

				try
				{
					ExecuteDeleteQuery(Tables.Ratings, $"UtoMID = \"{chapter.Value.MtoId}\"");
				}
				catch { }


				ExecuteStandartCmd(ratingCmd, new object[] { chapter.Value.MtoId, TRating + DRating + SRating, 2 });
			}
		}

		//Dictionary<int, List<float>> GetUserParametrData(Func<int,List<float>> getterData)
		//{			
		//	var users = ExecuteSelectQuery(Tables.Users, new[] { "UserID" }, "").First().ConvertAll((v) => int.Parse(v.ToString())).Distinct();
		//	var data = new Dictionary<int, List<float>>();
		//	foreach (var user in users)
		//	{
		//		var curData = new List<float>();
		//		var uToMIDs = ExecuteSelectQuery(Tables.UserToModule, new[] { "UtoMID" }, $"UserID = \"{user}\"").First().ConvertAll((v) => int.Parse(v.ToString())).Distinct();
		//		foreach (var uTiMID in uToMIDs)
		//		{
		//			var times = getterData(uTiMID);
		//				//ExecuteSelectQuery(Tables.Statistics, new[] { "TimeTask" }, $"UtoMID = \"{uTiMID}\"").First().ConvertAll((v) => float.Parse(v.ToString()));
		//			curData.AddRange(times);
		//		}
		//		data.Add(user, curData);
		//	};

		//	return data;
		//}

		private void UpdateRating(int modId)
		{
			var rows = ExecuteSelectLeftJoin(Tables.UserToModule, Tables.Statistics, new string[]
				{
					$"{Tables.UserToModule}.UtoMId", //0
					"ChapterId", //1
					"TopicId", //2
					"TaskId", //3
					"TimeTask", //4
					"CountSteps", //5
					"Done" //6
				}, $"{Tables.UserToModule.ToString()}.UtoMId = {Tables.Statistics.ToString()}.UtoMId", $"UserId = \"{AkinyiController.UserId}\" AND ModulId = \"{modId}\"");

			var topic = new Dictionary<int, DataHolder>();
			var chapt = new Dictionary<int, DataHolder>();

			//забиваем рейтинг по таскам
			foreach (var row in rows)
			{
				int topicID = row[2].ToString().ConvertToInt();
				int chapterID = row[1].ToString().ConvertToInt();
				if (!topic.Keys.Contains(topicID))
				{
					topic.Add(topicID, new DataHolder());
					topic.Last().Value.MtoId = row[0].ToString().ConvertToInt();
				}
				if (!chapt.Keys.Contains(chapterID))
				{
					chapt.Add(chapterID, new DataHolder());
					chapt.Last().Value.MtoId = row[0].ToString().ConvertToInt();
				}

				TimeSpan curTime = TimeSpan.FromMilliseconds(row[4].ToString().ConvertToInt());
				int curSteps = row[5].ToString().ConvertToInt();
				bool curDone = row[6].ToString().ConvertToInt() == 1;
				if (row[3].ConvertToInt() > 0)
				{
					int rat = RatingSystem.GetRating(curDone ? 100 : 0, curTime, curSteps, RatingSystemType.Task);
					ExecuteStandartCmd(ratingCmd, new object[] { row[0], rat, 0 });
				}

				topic[topicID].AddStats(curTime, curSteps, curDone);
				chapt[chapterID].AddStats(curTime, curSteps, curDone);
			}

			var ratings = ExecuteSelectLeftJoin(Tables.Ratings, Tables.UserToModule, new string[]
				{
					$"{Tables.UserToModule}.UtoMId", //0
					"UserId", //1
					"ChapterId", //2
					"TopicId", //3
					"TypeD", //4				
				}, $"{Tables.UserToModule.ToString()}.UtoMId = {Tables.Ratings.ToString()}.UtoMId", "");

			foreach (var top in topic)
			{
				if (top.Key < 0) continue;
				var ratTop = ratings.FirstOrDefault((v) =>
					v[1].ConvertToInt() == AkinyiController.UserId &&
					v[3].ConvertToInt() == top.Key &&
					v[4].ConvertToInt() == 1);
				int mainUtoM = top.Value.MtoId;
				if (ratTop != null)
				{
					mainUtoM = (int)ratTop[0];
				}

				ExecuteStandartCmd(ratingCmd, new object[] { mainUtoM, RatingSystem.GetRating(top.Value, RatingSystemType.Topic), 1 });
			}

			foreach (var chap in chapt)
			{
				if (chap.Key < 0) continue;
				var ratChap = ratings.FirstOrDefault((v) =>
					v[1].ConvertToInt() == AkinyiController.UserId &&
					v[2].ConvertToInt() == chap.Key &&
					v[4].ConvertToInt() == 2);
				int mainUtoM = chap.Value.MtoId;
				if (ratChap != null)
				{
					mainUtoM = ratChap[0].ConvertToInt();
					ExecuteDeleteQuery(Tables.Ratings, $"UtoMId = \"{mainUtoM}\"");
				}

				ExecuteStandartCmd(ratingCmd, new object[] { mainUtoM, RatingSystem.GetRating(chap.Value, RatingSystemType.Chapter), 2 });
			}
		}

		void ExecuteStandartCmd(string strCmd, IEnumerable<object> param)
		{
			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = strCmd;
				cmd.AddParameters(param);
				cmd.ExecuteNonQuery();
			}
		}

		internal void AddModul(AkinyiModuleInfo key)
		{
			using (var cmd = con.CreateCommand())
			{
				cmd.CommandText = modulCmd;
				cmd.Parameters.Add(new SQLiteParameter()
				{
					Value = key.Title,
					DbType = DbType.String
				});
				cmd.Parameters.Add(new SQLiteParameter()
				{
					Value = key.Version,
					DbType = DbType.String
				});
				cmd.ExecuteNonQuery();
			}
		}

		private void RecreateDBStruct()
		{
			using (var cmdSql = con.CreateCommand())
			{
				cmdSql.CommandText = File.ReadAllText(script);
				cmdSql.ExecuteNonQuery();
			}
		}

		public void Dispose()
		{
			try
			{
				con.Close();
			}
			finally
			{
				con.Dispose();
			}
		}
	}
}