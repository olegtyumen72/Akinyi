﻿using System.Collections.Generic;

namespace ApiAkinyi
{
	/// <summary>
	/// 0 - повторить, 1 - так же интересно будет, 2 - можно изучать углублённо
	/// </summary>
	public enum TypeRecom { Repeat=0, Simular=1, Deeper=3 }

	public class RecommendDatas
	{
		/// <summary>
		/// Идентификатор модуля
		/// </summary>
		public string ModuleName { get; set; }

		public List<AkinyiRecomData> Datas { get; set; } = new List<AkinyiRecomData>();

		public class AkinyiRecomData
		{
			/// <summary>
			/// Название главы. Не пустое
			/// </summary>
			public string Chapter { get; set; }
			/// <summary>
			/// Название темы. Может быть пустым
			/// </summary>
			public string Topic { get; set; }
			/// <summary>
			/// Название задания. Может быть пустым
			/// </summary>
			public string Task { get; set; }
			/// <summary>
			/// Тип рекомендации: повторить\так же будет интересно\углублённое изучение
			/// </summary>
			public TypeRecom RecomType;
		}
	}
}