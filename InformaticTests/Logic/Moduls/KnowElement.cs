﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace InformaticTests.Logic.Moduls
{
	public interface IKnowElement
	{
		UIElement GetView();
	}

	public abstract class KnowElement<T>: IKnowElement
	{
		public T Element { get; set; }

		public abstract UIElement GetView();		
	}

	public class KnowString : KnowElement<string>
	{
		public override UIElement GetView()
		{
			return new TextBlock() { Text = Element, TextWrapping = TextWrapping.Wrap, Padding = new Thickness(3) };
		}
	}

	public class KnowBitmap : KnowElement<string>
	{
		public override UIElement GetView()
		{
			return new Image() { Source = new BitmapImage(new Uri(Element, UriKind.Absolute)) };
		}		
	}
}
