﻿using PhysicModule.GUI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections;
using PhysicModule.Interfaces;

namespace PhysicModule.Models.Text
{
	public class SmartList : List<TreeComp>
	{
		public event Action<TreeComp> OnAdd;

		public void Add(TreeComp item)
		{
			if (item != null) OnAdd?.Invoke(item);
			base.Add(item);
		}
	}

	public interface TreeComp
	{
		string Title { get; set; }
	}

	public abstract class Branch : TreeComp
	{

		SmartList childInfo = new SmartList();

		public string Title { get; set; } = "";
		public SmartList ChildInfo { get => childInfo; set { UpdateList(value); SignList(value); childInfo = value; } }

		protected abstract void UpdateList(SmartList list);

		protected abstract void SignList(SmartList list);
	}

	public abstract class Leaf : TreeComp
	{
		public string Title { get; set; } = "";
		public Action<Physic> DoSmth { get; set; }
		public Stopwatch Timer { get; set; } = new Stopwatch();
		public int Steps { get; set; } = 0;
		public bool Done { get; set; } = false;

		public event Action<string, Stopwatch, int, bool> TaskEnd;

		public Leaf()
		{

		}

		public Leaf(string name, Action<Physic> dodo)
		{
			Title = name;
			DoSmth = dodo;
		}

		public void Start()
		{
			Timer.Start();
		}

		public void End()
		{
			Timer.Stop();
			TaskEnd?.Invoke(Title, Timer, Steps, Done);
		}

		public void AddStep()
		{
			Steps++;
		}

		public Func<List<DrawElementBase>, TopicTask ,bool> CheckTask { get; set; }
	}

	public class Chapter : Branch
	{

		public event Action<string, string, string, Stopwatch, int, bool> ChapterTaskEnd;

		public Chapter()
		{
			ChildInfo.OnAdd += (v) =>
			{
				if (v is Topic)
					(v as Topic).TopickTaskEnd += (topic, task, v1, v2, v3) =>
					{
						ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
					};
				if (v is Chapter)
					(v as Chapter).ChapterTaskEnd += (chap, topic, task, v1, v2, v3) =>
					{
						ChapterTaskEnd?.Invoke(chap, topic, task, v1, v2, v3);
					};
			};
		}

		protected override void UpdateList(SmartList list)
		{
			list.OnAdd += (v) =>
			{
				(v as Topic).TopickTaskEnd += (topic, task, v1, v2, v3) =>
				{
					ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
				};
			};
		}

		protected override void SignList(SmartList list)
		{
			foreach (Topic el in list)
			{
				el.TopickTaskEnd += (topic, task, v1, v2, v3) =>
				{
					ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
				};
			}
		}
	}

	public class Topic : Branch
	{
		public Topic()
		{

			ChildInfo.OnAdd += (v) =>
			{
				(v as Leaf).TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			};

		}

		public event Action<string, string, Stopwatch, int, bool> TopickTaskEnd;

		protected override void SignList(SmartList list)
		{
			foreach (TopicTask el in list)
			{
				el.TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			}
		}

		protected override void UpdateList(SmartList list)
		{
			list.OnAdd += (v) =>
			{
				(v as Leaf).TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			};
		}
	}

	public class TopicTask : Leaf
	{
		public TopicTask() : base()
		{

		}

		public TopicTask(string name, Action<Physic> dodo) : base(name, dodo)
		{
		}
	}
}
