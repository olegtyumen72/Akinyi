DROP TABLE IF EXISTS Ratings;
DROP TABLE IF EXISTS Statistics;
DROP TABLE IF EXISTS UserToModule;
DROP TABLE IF EXISTS ModulsToTags;
DROP TABLE IF EXISTS Moduls;
DROP TABLE IF EXISTS Chapters;
DROP TABLE IF EXISTS Topics;
DROP TABLE IF EXISTS Tasks;
DROP TABLE IF EXISTS Users;
DROP TABLE IF EXISTS Tags;

CREATE TABLE Users (
	UserID integer PRIMARY KEY AUTOINCREMENT,
	Login text,
	Pas text
);

CREATE TABLE Statistics (
	StatID integer PRIMARY KEY AUTOINCREMENT,
	UtoMID integer,
	TimeTask real,
	CountSteps integer,
	Done integer,
	FOREIGN KEY(UtoMID) REFERENCES UserToModule(UtoMID)
);

CREATE TABLE Chapters (
	ChapterID integer PRIMARY KEY AUTOINCREMENT,
	Chapter text
);

CREATE TABLE Topics (
	TopicID integer PRIMARY KEY AUTOINCREMENT,
	Topic text
);

CREATE TABLE Tasks (
	TaskID integer PRIMARY KEY AUTOINCREMENT,
	Task text
);

CREATE TABLE Moduls (
	ModulID integer PRIMARY KEY AUTOINCREMENT,
	Module text,
	Version text
);

CREATE TABLE Ratings (
	UtoMID integer,
	Rating integer,
	TypeD integer,
	FOREIGN KEY(UtoMId) REFERENCES UserToModule(UtoMId)
);

CREATE TABLE UserToModule (
	UtoMID integer PRIMARY KEY AUTOINCREMENT,
	UserID integer,
	ModulID integer,
	ChapterID integer,
	TopicID integer,
	TaskID integer,
	FOREIGN KEY(UserId) REFERENCES Users(UserId), 
	FOREIGN KEY(ModulId) REFERENCES Moduls(ModulId), 
	FOREIGN KEY(ChapterId) REFERENCES Chapters(ChapterId), 
	FOREIGN KEY(TopicId) REFERENCES Topics(TopicId), 
	FOREIGN KEY(TaskId) REFERENCES Tasks(TaskId)
);

CREATE TABLE Tags (
	TagID integer PRIMARY KEY AUTOINCREMENT,
	Tag text
);

CREATE TABLE ModulsToTags (
	ModulToTag integer PRIMARY KEY AUTOINCREMENT,
	ModulID integer,
	TagID integer,
	FOREIGN KEY(ModulID) REFERENCES Moduls(ModulId), 
	FOREIGN KEY(TagID) REFERENCES Tags(TagID)
);