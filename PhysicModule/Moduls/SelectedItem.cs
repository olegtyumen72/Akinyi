﻿using PhysicModule.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;

namespace PhysicModule.Moduls
{
	internal class SelectedItem
	{

		public DrawElementBase Item;
		public Shape Selection;

		public SelectedItem()
		{

		}

		public SelectedItem(DrawElementBase iti, Shape shapi)
		{
			Item = iti;
			Selection = shapi;
		}

	}
}
