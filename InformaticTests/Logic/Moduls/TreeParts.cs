﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections;

namespace InformaticTests.Logic.Moduls.Text
{
	public class SmartList : List<ITreeComp>
	{
		public event Action<ITreeComp> OnAdd;

		public void Add(ITreeComp item)
		{
			if (item != null) OnAdd?.Invoke(item);
			base.Add(item);
		}
	}

	public interface ITreeComp
	{
		string Title { get; set; }

		TableElement TableView { get; set; }
	}

	public abstract class Branch : ITreeComp
	{

		SmartList childInfo = new SmartList();

		public string Title { get; set; } = "";
		public SmartList ChildInfo { get => childInfo; set { UpdateList(value); SignList(value); childInfo = value; } }

		public TableElement TableView { get; set; } = new TitleElement();

		protected abstract void UpdateList(SmartList list);

		protected abstract void SignList(SmartList list);
	}

	public abstract class Leaf : ITreeComp
	{
		TableElement elem;

		public string Title { get; set; } = "";
		public TableElement TableView
		{
			get => elem;
			set
			{
				elem = value;
				if (value is TaskElement tmp)
				{
					tmp.TaskEnd += End;
					tmp.TaskStart += Start;
				}
			}
		}
		public Stopwatch Timer { get; set; } = new Stopwatch();

		public event Action<string, Stopwatch, int, bool> TaskEnd;

		public Leaf()
		{

		}

		public Leaf(string name)
		{
			Title = name;
		}

		public void Start()
		{
			Timer.Start();
		}

		public void End(TaskElement obj)
		{
			Timer.Stop();
			TaskEnd?.Invoke(Title, Timer, obj.GetChoosen(), obj.IsRight);
		}

		public Func<List<object>, TopicTask, bool> CheckTask { get; set; }
	}

	public class Chapter : Branch
	{

		public event Action<string, string, string, Stopwatch, int, bool> ChapterTaskEnd;

		public Chapter()
		{
			ChildInfo.OnAdd += (v) =>
			{
				if (v is Topic)
					(v as Topic).TopickTaskEnd += (topic, task, v1, v2, v3) =>
					{
						ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
					};
				if (v is Chapter)
					(v as Chapter).ChapterTaskEnd += (chap, topic, task, v1, v2, v3) =>
					{
						ChapterTaskEnd?.Invoke(chap, topic, task, v1, v2, v3);
					};
			};
		}

		protected override void UpdateList(SmartList list)
		{
			list.OnAdd += (v) =>
			{
				(v as Topic).TopickTaskEnd += (topic, task, v1, v2, v3) =>
				{
					ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
				};
			};
		}

		protected override void SignList(SmartList list)
		{
			foreach (Topic el in list)
			{
				el.TopickTaskEnd += (topic, task, v1, v2, v3) =>
				{
					ChapterTaskEnd?.Invoke(Title, topic, task, v1, v2, v3);
				};
			}
		}
	}

	public class Topic : Branch
	{
		public Topic()
		{

			ChildInfo.OnAdd += (v) =>
			{
				(v as Leaf).TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			};

		}

		public event Action<string, string, Stopwatch, int, bool> TopickTaskEnd;

		protected override void SignList(SmartList list)
		{
			foreach (TopicTask el in list)
			{
				el.TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			}
		}

		protected override void UpdateList(SmartList list)
		{
			list.OnAdd += (v) =>
			{
				(v as Leaf).TaskEnd += (name, v1, v2, v3) =>
				{
					TopickTaskEnd?.Invoke(Title, name, v1, v2, v3);
				};
			};
		}
	}

	public class TopicTask : Leaf
	{
		public TopicTask() : base()
		{

		}

		public TopicTask(string name, Action dodo) : base(name)
		{
		}
	}
}
