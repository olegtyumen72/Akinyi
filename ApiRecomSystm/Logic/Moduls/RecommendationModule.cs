﻿using ApiAkinyi;
using NReco.CF.Taste.Impl.Neighborhood;
using NReco.CF.Taste.Impl.Recommender;
using NReco.CF.Taste.Impl.Similarity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRecomSystm.Logic.Moduls
{
	class RecommendationModule
	{		
		public RecommendationModule()
		{

		}

		//public RecommendationModule(List<AkinyiModuleInfo> modsInfo, List<IEnumerable<ColdStartUser>> modsUsrsColdStart, List<List<string>[]> allMainModulInfo, List<IAkinyiModule> moduls)
		//{
		//	db = dB;
		//	if (db.IsFresh)
		//	{
		//		for (int i = 0; i < modsInfo.Count; i++)
		//		{
		//			db.Add("Chapters", new List<string> { "Chapter" }, new List<List<string>> { allMainModulInfo[i][0] });
		//			db.Add("Topics", new List<string> { "Topic" }, new List<List<string>> { allMainModulInfo[i][1] });
		//			db.Add("Tasks", new List<string> { "Task" }, new List<List<string>> { allMainModulInfo[i][2] });

		//			db.AddModul(modsInfo[i]);
		//			db.AddColdUsers(modsUsrsColdStart[i], modsInfo[i].Title);
		//		}
		//	}
		//}		

		public RecommendDatas GetReccomendations(string modTitle)
		{
			var res = new RecommendDatas()
			{
				ModuleName = modTitle
			};

			var dataModel = AkinyiController.GetModel();
			var similarityI = new LogLikelihoodSimilarity(dataModel);
			//var sim2 = new Similarity(dataModel);		


			var recommender = new GenericItemBasedRecommender(dataModel, similarityI);
			var recommendItems = recommender.Recommend(AkinyiController.UserId, 3);

			if (recommendItems.Count > 0)
			{
				foreach (var item in recommendItems)
				{
					res.Datas.Add(new RecommendDatas.AkinyiRecomData()
					{
						RecomType = TypeRecom.Simular,
						Chapter = AkinyiController.GetChapterName((int)item.GetItemID())
					});
				}

			}
			else
			{
				var recommender2 = new GenericBooleanPrefItemBasedRecommender(dataModel, similarityI);
				recommendItems = recommender2.Recommend(AkinyiController.UserId, 3);

				if (recommendItems.Count > 0)
				{
					foreach (var item in recommendItems)
					{
						res.Datas.Add(new RecommendDatas.AkinyiRecomData()
						{
							RecomType = TypeRecom.Simular,
							Chapter = AkinyiController.GetChapterName((int)item.GetItemID())
						});
					}
				}
			}

			return res;
		}	
	}
}
