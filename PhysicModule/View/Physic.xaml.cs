﻿using PhysicModule.Interfaces;
using PhysicModule.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ApiAkinyi;
using PhysicModule.Logic.Moduls;
using System.Collections.ObjectModel;
using PhysicModule.Models;
using PhysicModule.Models.Text;
using System.Diagnostics;

namespace PhysicModule.GUI
{
	/// <summary>
	/// Interaction logic for UserControl1.xaml
	/// </summary>
	public partial class Physic : UserControl
	{

		CommandBinding LoadItem = new CommandBinding(PhysicModelView.Load);
		CommandBinding Save = new CommandBinding(PhysicModelView.Save);
		CommandBinding SaveAs = new CommandBinding(PhysicModelView.SaveAs);
		CommandBinding ExportProg = new CommandBinding(PhysicModelView.LoadProgress);
		CommandBinding ImportProg = new CommandBinding(PhysicModelView.SaveProgress);
		CommandBinding CheckTask = new CommandBinding(PhysicModelView.CheckTask);
		CommandBinding RouteElem = new CommandBinding(PhysicModelView.RouteElemCmd);

		public List<string> AllChapters = new List<string>();
		public List<string> AllTopic = new List<string>();
		public List<string> AllTasks = new List<string>();

		ObservableCollection<Chapter> recom;
		Chapter rec;

		void InitCommands()
		{
			LoadItem.Executed += LoadItem_Executed;
			Save.Executed += LoadItem_Executed;
			SaveAs.Executed += LoadItem_Executed;
			ExportProg.Executed += LoadItem_Executed;
			ImportProg.Executed += LoadItem_Executed;
			CheckTask.Executed += CheckTask_Executed;
			RouteElem.Executed += RouteElem_Executed;
			CommandBindings.Add(LoadItem);
			CommandBindings.Add(Save);
			CommandBindings.Add(SaveAs);
			CommandBindings.Add(ExportProg);
			CommandBindings.Add(ImportProg);
			CommandBindings.Add(CheckTask);
			CommandBindings.Add(RouteElem);
		}

		private void RouteElem_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			SchemeField.mLogic.RouteCure();
		}

		private void CheckTask_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			var res = SchemeField.mLogic.InitChec(curTask as TopicTask);
			if (res)
			{
				var tmp = curTask.Timer.Elapsed;
				MessageBox.Show($"Время: {tmp.Hours}:{tmp.Minutes}:{tmp.Seconds}.{tmp.Milliseconds} Количество шагов: {curTask.Steps}" , "Задание выполненно", MessageBoxButton.OK);
			}
			else
			{
				MessageBox.Show($"Перечитайте теорию и попробуйте ещё раз. Время: {curTask.Timer.Elapsed} Количество шагов: {curTask.Steps}", "Задание решено не правильно", MessageBoxButton.OK);
			}
		}

		public Physic()
		{
			var qwe = ViewModel = new PhysicModelView();
			InitializeComponent();
			InitCommands();			

			
			DataContext = qwe;
			TreeTasks.ItemsSource = qwe.Chapters;

			recom = new ObservableCollection<Chapter>
			{
				new Chapter()
				{
					Title = "Интересные"
				}
			};
			rec = recom.Last();

			foreach (var chap in qwe.Chapters)
			{
				if (!AllChapters.Contains(chap.Title))
					AllChapters.Add(chap.Title);
				foreach (Topic topic in chap.ChildInfo)
				{
					if (!AllTopic.Contains(topic.Title))
						AllTopic.Add(topic.Title);
					foreach (TopicTask task in topic.ChildInfo)
					{
						if (!AllTasks.Contains(task.Title))
							AllTasks.Add(task.Title);
					}
				}
			}

			foreach(var chapt in qwe.Chapters)
			{
				chapt.ChapterTaskEnd += Physic_ChapterTaskEnd;
			}

			//Loaded += (v, o) =>
			//{
			//	var rawData = new RawDatas();
			//	rawData.MDatas.Add(new ModulData()
			//	{
			//		MChapter = AllChapters[0],
			//		MIsDone = true,
			//		MRelatedTime = TimeSpan.FromMinutes(90),
			//		MSteps = 600
			//	});
			//	rawData.MDatas.Add(new ModulData()
			//	{
			//		MChapter = AllChapters[1],
			//		MIsDone = false,
			//		MRelatedTime = TimeSpan.FromMinutes(0),
			//		MSteps = 0
			//	});
			//	MainModule.instance.PluginUpdate(rawData);
			//};		
		}

		private void Physic_ChapterTaskEnd(string chapt, string topic, string task, Stopwatch time, int steps, bool done)
		{
			var rawData = new RawDatas();

			rawData.MDatas.Add(new ModulData()
			{
				MChapter = chapt,
				MTopic = topic,
				MTask = task,
				MRelatedTime = time.Elapsed,
				MSteps = steps,
				MIsDone = done
			});
			
			MainModule.instance.PluginUpdate(rawData);
		}

		private void LoadItem_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			MessageBox.Show("OK");
		}

		private void AddDrawableItem(object sender, MouseButtonEventArgs e)
		{

			var tmp = sender as ListView;
			if (tmp.SelectedItem == null) return;
			SchemeField.mLogic.AddNewElement((tmp.SelectedItem as DrawElementBase).Clone());

			tmp.UnselectAll();

		}		

		internal void SetRecommendations(List<RecommendDatas.AkinyiRecomData> datas)
		{
			RecTab.IsEnabled = true;
			rec.ChildInfo.Clear();			

			foreach(var dat in datas)
			{
				if (dat.RecomType == TypeRecom.Simular)
				{
					var chap = ViewModel.Chapters.First((v) => v.Title == dat.Chapter);
					rec.ChildInfo.Add(chap);
				}
			}

			TreeRecom.ItemsSource = recom;
			TreeRecom.DataContext = recom;
		}

		Leaf curTask;

		private void TreeTasks_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			if (TreeTasks.SelectedItem is Leaf task)
			{
				curTask = task;
				SchemeField.mLogic.NewStep += task.AddStep;
				SchemeField.mLogic.TaskEnd += task.End;
				SchemeField.mLogic.TaskStart += task.Start;
				SchemeField.mLogic.CheckTask += task.CheckTask;
				task.DoSmth(this);				
			}
		}	
	}
}
