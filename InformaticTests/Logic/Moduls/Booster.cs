﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Xml;

namespace InformaticTests.Logic.Moduls
{
	public static class Booster
	{		

		public static T Clone<T>(UIElement element)
		{
			string gridXaml = XamlWriter.Save(element);
			StringReader stringReader = new StringReader(gridXaml);
			XmlReader xmlReader = XmlReader.Create(stringReader);
			return (T)XamlReader.Load(xmlReader);
		}

		public static T Clone<T>(T element)
		{
			string gridXaml = XamlWriter.Save(element);
			StringReader stringReader = new StringReader(gridXaml);
			XmlReader xmlReader = XmlReader.Create(stringReader);
			return (T)XamlReader.Load(xmlReader);
		}

		public static void SetDock(this UIElement elem, Dock dck)
		{
			DockPanel.SetDock(elem, dck);
		}

	}
}
